-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Okt 2019 pada 05.04
-- Versi Server: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil`
--

CREATE TABLE IF NOT EXISTS `hasil` (
  `id` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `id_sesi` int(10) NOT NULL,
  `nilai` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil`
--

INSERT INTO `hasil` (`id`, `nip`, `id_sesi`, `nilai`) VALUES
(2, '08999', 10, 5),
(3, '0999', 11, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jawaban`
--

CREATE TABLE IF NOT EXISTS `jawaban` (
  `id_jawaban` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `id_sesi` int(10) NOT NULL,
  `jawaban` varchar(2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jawaban`
--

INSERT INTO `jawaban` (`id_jawaban`, `nip`, `id_soal`, `id_sesi`, `jawaban`) VALUES
(2, '08999', 28, 10, 'A'),
(3, '0999', 29, 11, 'G'),
(4, '0999', 29, 11, 'F'),
(5, '0999', 29, 11, 'G'),
(6, '0777', 29, 11, 'G'),
(7, '0777', 29, 11, 'G'),
(8, '0777', 29, 11, 'G');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jawaban_tiu`
--

CREATE TABLE IF NOT EXISTS `jawaban_tiu` (
  `id_jawaban` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `id_sesi` int(10) NOT NULL,
  `jawaban` varchar(2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jawaban_tiu`
--

INSERT INTO `jawaban_tiu` (`id_jawaban`, `nip`, `id_soal`, `id_sesi`, `jawaban`) VALUES
(2, '2', 24, 7, 'D'),
(3, '2', 1, 7, 'B'),
(4, '2', 23, 7, 'B'),
(5, '2', 26, 7, 'B'),
(6, '2', 25, 7, 'A'),
(7, '2', 1, 7, 'A'),
(8, '2', 2, 7, 'A'),
(9, '2', 1, 7, 'E'),
(10, '2', 1, 7, 'E'),
(11, '2', 1, 7, 'D'),
(12, '2', 1, 7, NULL),
(13, '2', 1, 7, 'B'),
(14, '2', 1, 7, 'C'),
(15, '2', 1, 7, 'D'),
(16, '2', 1, 7, 'B'),
(17, '2', 1, 7, 'C'),
(18, '2', 1, 7, NULL),
(19, '2', 3, 7, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_soal`
--

CREATE TABLE IF NOT EXISTS `jenis_soal` (
  `id` int(2) NOT NULL,
  `jenis_soal` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_soal`
--

INSERT INTO `jenis_soal` (`id`, `jenis_soal`) VALUES
(1, 'TWK'),
(2, 'Tes Numerik - Hitungan (Aritmatika)'),
(3, 'Test Karakteristik Pribadi (TKP)'),
(4, 'TIU 5'),
(5, 'PAPI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_ujian`
--

CREATE TABLE IF NOT EXISTS `kategori_ujian` (
  `id` int(10) NOT NULL,
  `kategori` text NOT NULL,
  `waktu_pengerjaan` int(8) NOT NULL,
  `jlh_soal` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_ujian`
--

INSERT INTO `kategori_ujian` (`id`, `kategori`, `waktu_pengerjaan`, `jlh_soal`) VALUES
(5, 'Ujian Kedinasan', 1800, 100),
(10, 'Ujian Kenaikan Pangkat', 3600, 100),
(11, 'tiu', 6000, 3),
(12, 'PAPI', 1500, 90);

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Struktur dari tabel `manajemen_ujian`
--

CREATE TABLE IF NOT EXISTS `manajemen_ujian` (
  `id` int(11) NOT NULL,
  `id_kategori_ujian` int(10) NOT NULL,
  `id_jenis_soal` int(2) NOT NULL,
  `persentase` float NOT NULL,
  `passing_grade` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `manajemen_ujian`
--

INSERT INTO `manajemen_ujian` (`id`, `id_kategori_ujian`, `id_jenis_soal`, `persentase`, `passing_grade`) VALUES
(13, 5, 1, 10, 50),
(14, 5, 2, 10, 50),
(15, 10, 1, 10, 10),
(16, 11, 4, 1, 10),
(17, 12, 5, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nama_lengkap` text NOT NULL,
  `id_sesi` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `nama_lengkap`, `id_sesi`) VALUES
(7, '1', 'budi', 7),
(8, '2', 'badu', 7),
(9, '3', 'anto', 8),
(37, '10', 'Rizky', 8),
(38, '08999', 'robby', 10),
(39, '0999', 'robby', 11),
(40, '0777', 'wahyu', 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sesi`
--

CREATE TABLE IF NOT EXISTS `sesi` (
  `id_sesi` int(11) NOT NULL,
  `nama_sesi` varchar(50) NOT NULL,
  `waktu` datetime NOT NULL,
  `waktu_akhir` datetime NOT NULL,
  `id_kategori_ujian` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sesi`
--

INSERT INTO `sesi` (`id_sesi`, `nama_sesi`, `waktu`, `waktu_akhir`, `id_kategori_ujian`) VALUES
(7, 'Sesi 1', '2019-09-28 08:00:00', '2019-09-28 23:00:00', 5),
(8, 'Sesi 1', '2019-08-28 08:00:00', '2019-08-28 12:00:00', 10),
(9, 'Sesi 2', '2019-08-27 13:00:00', '2019-08-27 15:00:00', 5),
(10, 'Sesi 1', '2019-09-30 08:00:00', '2019-09-30 18:00:00', 11),
(11, 'Sesi 1 Papi', '2019-09-30 08:00:00', '2019-09-30 10:00:00', 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal_dinas`
--

CREATE TABLE IF NOT EXISTS `soal_dinas` (
  `id` int(11) NOT NULL,
  `soal` text NOT NULL,
  `jawaban_a` text NOT NULL,
  `bobot_a` varchar(3) DEFAULT NULL,
  `jawaban_b` text NOT NULL,
  `bobot_b` varchar(3) DEFAULT NULL,
  `jawaban_c` text NOT NULL,
  `bobot_c` float DEFAULT NULL,
  `jawaban_d` text NOT NULL,
  `bobot_d` float DEFAULT NULL,
  `jawaban_e` text NOT NULL,
  `bobot_e` float DEFAULT NULL,
  `kunci_jawaban` enum('A','B','C','D','E','') DEFAULT NULL,
  `id_jenis_soal` int(2) NOT NULL,
  `bobot` float DEFAULT NULL,
  `status` enum('single','multiple','papi') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `soal_dinas`
--

INSERT INTO `soal_dinas` (`id`, `soal`, `jawaban_a`, `bobot_a`, `jawaban_b`, `bobot_b`, `jawaban_c`, `bobot_c`, `jawaban_d`, `bobot_d`, `jawaban_e`, `bobot_e`, `kunci_jawaban`, `id_jenis_soal`, `bobot`, `status`) VALUES
(1, 'Kata depan yang ditulis secara benar terdapat pada .... ', 'bersembunyi dibalik pintu', '0', 'menjilat keatas', '0', 'datang kemari', NULL, 'pergi keluar negeri', NULL, 'tertera dibawah ini', NULL, 'A', 1, 5, 'single'),
(2, '<p>Tanda hubung yang digunakan secara benar terdapat padas</p>\r\n', '<p>Adat&nbsp;sopan-santuns</p>\r\n', '11', '<p>Muka&nbsp;merah-padams</p>\r\n', '22', '<p>Kaum&nbsp;muda-belias</p>\r\n', 33, '<p>Rumah&nbsp;porak-porandas</p>\r\n', 44, '<p>Penuh&nbsp;kasih-sayangs</p>\r\n', 55, '', 2, 5, 'multiple'),
(23, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Tanda koma yang digunakan secara tepat terdapat pada</span></span></span></p>\r\n', '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Terdakwa menurut hakiM, terbuk1i bersalah.</span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Perusahaan itu bangkrut, karena ulah direk1ur utamanya.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:18.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Dia belum berhasil, meskipun sudah bekerja keras.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Jika tidak segera diberantas, peredaran narkoba akan semakin merajalela.</span></span></span></span></span></span></p>\r\n', NULL, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Pengalaman hidup itu, guru yang baik.</span></span></span></p>\r\n', NULL, 'B', 1, 5, 'single'),
(24, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Tanda koma yang digunakan secara tepat terdapat pada</span></span></span></p>\r\n', '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Terdakwa menurut hakiM, terbuk1i bersalah.</span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Perusahaan itu bangkrut, karena ulah direk1ur utamanya.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:18.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Dia belum berhasil, meskipun sudah bekerja keras.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Jika tidak segera diberantas, peredaran narkoba akan semakin merajalela.</span></span></span></span></span></span></p>\r\n', NULL, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Pengalaman hidup itu, guru yang baik.</span></span></span></p>\r\n', NULL, 'B', 1, 5, 'single'),
(25, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Tanda koma yang digunakan secara tepat terdapat pada</span></span></span></p>\r\n', '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Terdakwa menurut hakiM, terbuk1i bersalah.</span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Perusahaan itu bangkrut, karena ulah direk1ur utamanya.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:18.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Dia belum berhasil, meskipun sudah bekerja keras.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Jika tidak segera diberantas, peredaran narkoba akan semakin merajalela.</span></span></span></span></span></span></p>\r\n', NULL, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Pengalaman hidup itu, guru yang baik.</span></span></span></p>\r\n', NULL, 'B', 1, 5, 'single'),
(26, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Tanda koma yang digunakan secara tepat terdapat pada</span></span></span></p>\r\n', '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Terdakwa menurut hakiM, terbuk1i bersalah.</span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Perusahaan itu bangkrut, karena ulah direk1ur utamanya.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:18.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Dia belum berhasil, meskipun sudah bekerja keras.</span></span></span></span></span></span></p>\r\n', NULL, '<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:11pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Jika tidak segera diberantas, peredaran narkoba akan semakin merajalela.</span></span></span></span></span></span></p>\r\n', NULL, '<p><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#191919">Pengalaman hidup itu, guru yang baik.</span></span></span></p>\r\n', NULL, 'B', 1, 5, 'single'),
(28, '<p><img alt="" src="/cat/assets/admin/js/lib/ckeditor12/kcfinder/upload/files/Capture.PNG" style="height:125px; width:391px" /></p>\r\n', '<p><img alt="" src="/cat/assets/admin/js/lib/ckeditor12/kcfinder/upload/files/a.PNG" style="height:88px; width:55px" /></p>\r\n', NULL, '<p><img alt="" src="/cat/assets/admin/js/lib/ckeditor12/kcfinder/upload/files/bb.PNG" style="height:98px; width:72px" /></p>\r\n', NULL, '<p><img alt="" src="/cat/assets/admin/js/lib/ckeditor12/kcfinder/upload/files/c.PNG" style="height:65px; width:52px" /></p>\r\n', NULL, '<p><img alt="" src="/cat/assets/admin/js/lib/ckeditor12/kcfinder/upload/files/d.PNG" style="height:86px; width:70px" /></p>\r\n', NULL, '<p><img alt="" src="/cat/assets/admin/js/lib/ckeditor12/kcfinder/upload/files/e.PNG" style="height:106px; width:104px" /></p>\r\n', NULL, 'A', 4, 5, 'single'),
(29, '', '<p>Saya seorang pekerja keras</p>\r\n', 'G', '<p>Saya bukan seorang pemurung</p>\r\n', 'C', '', NULL, '', NULL, '', NULL, NULL, 5, NULL, 'papi'),
(30, '', '<p>BBV</p>\r\n', 'B', '<p>MMM</p>\r\n', 'C', '', NULL, '', NULL, '', NULL, NULL, 5, NULL, 'papi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nip` (`nip`),
  ADD KEY `id_kategori_ujian` (`id_sesi`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`),
  ADD KEY `nip` (`nip`),
  ADD KEY `id_soal` (`id_soal`),
  ADD KEY `id_kategori_ujian` (`id_sesi`);

--
-- Indexes for table `jawaban_tiu`
--
ALTER TABLE `jawaban_tiu`
  ADD PRIMARY KEY (`id_jawaban`),
  ADD KEY `nip` (`nip`),
  ADD KEY `id_soal` (`id_soal`),
  ADD KEY `id_kategori_ujian` (`id_sesi`);

--
-- Indexes for table `jenis_soal`
--
ALTER TABLE `jenis_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_ujian`
--
ALTER TABLE `kategori_ujian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `manajemen_ujian`
--
ALTER TABLE `manajemen_ujian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_soal` (`id_jenis_soal`),
  ADD KEY `id_kategori_soal` (`id_kategori_ujian`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori_ujian` (`id_sesi`);

--
-- Indexes for table `sesi`
--
ALTER TABLE `sesi`
  ADD PRIMARY KEY (`id_sesi`),
  ADD KEY `id_kategori_ujian` (`id_kategori_ujian`);

--
-- Indexes for table `soal_dinas`
--
ALTER TABLE `soal_dinas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bagian_soal` (`id_jenis_soal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `jawaban_tiu`
--
ALTER TABLE `jawaban_tiu`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `jenis_soal`
--
ALTER TABLE `jenis_soal`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kategori_ujian`
--
ALTER TABLE `kategori_ujian`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `manajemen_ujian`
--
ALTER TABLE `manajemen_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `sesi`
--
ALTER TABLE `sesi`
  MODIFY `id_sesi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `soal_dinas`
--
ALTER TABLE `soal_dinas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hasil`
--
ALTER TABLE `hasil`
  ADD CONSTRAINT `hasil_ibfk_2` FOREIGN KEY (`id_sesi`) REFERENCES `sesi` (`id_sesi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jawaban`
--
ALTER TABLE `jawaban`
  ADD CONSTRAINT `jawaban_ibfk_2` FOREIGN KEY (`id_soal`) REFERENCES `soal_dinas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `manajemen_ujian`
--
ALTER TABLE `manajemen_ujian`
  ADD CONSTRAINT `manajemen_ujian_ibfk_1` FOREIGN KEY (`id_jenis_soal`) REFERENCES `jenis_soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `manajemen_ujian_ibfk_2` FOREIGN KEY (`id_kategori_ujian`) REFERENCES `kategori_ujian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `pegawai_ibfk_1` FOREIGN KEY (`id_sesi`) REFERENCES `sesi` (`id_sesi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sesi`
--
ALTER TABLE `sesi`
  ADD CONSTRAINT `sesi_ibfk_1` FOREIGN KEY (`id_kategori_ujian`) REFERENCES `kategori_ujian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `soal_dinas`
--
ALTER TABLE `soal_dinas`
  ADD CONSTRAINT `soal_dinas_ibfk_2` FOREIGN KEY (`id_jenis_soal`) REFERENCES `jenis_soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
