<?php

	class hasil extends CI_Controller
	{
		public function __construct(){
			parent::__construct();

			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
		}

		public function index()
		{
			
			$query = $this->db->query("select kategori as kategori_ujian , id as id_kategori_ujian from kategori_ujian");
			$this->slice->with("data",$query->result());
			$this->slice->with("title","Hasil CAT");
			view("hasil_view");

		}

		public function details($kategori_ujian)
		{
			$query = $this->db->query("select * from sesi where id_kategori_ujian = '$kategori_ujian'  ");
			$this->slice->with("data",$query->result());
			$this->slice->with("title","Hasil Ujian");
			view("hasil_details");
		}

		public function search_by_date()
		{
			$tgl_mulai = $this->input->post("tgl_mulai");

			$query = $this->db->query("select * from sesi where DATE(waktu) = '$tgl_mulai'" );
			$this->slice->with("data",$query->result());
			$this->slice->with("title","Hasil Ujian");
			view("hasil_details");
		}
		public function details_pegawai($kategori_ujian,$id_sesi)
		{

			$kategori = array("A","B","C","D","E","F","G","I","K","L","N","O","P","R","S","T","V","W","X","Z");
			$query = $this->db->query("select c.nip,sum(a.bobot) as bobot , e.jenis_soal, d.id_sesi from soal_dinas a join jawaban b on a.id=b.id_soal join pegawai c on c.nip=b.nip join sesi d on c.id_sesi=d.id_sesi join jenis_soal e on a.id_jenis_soal=e.id join manajemen_ujian f on f.id_jenis_soal=e.id where b.id_sesi ='$id_sesi' and d.id_kategori_ujian='$kategori_ujian' group by e.id,c.nip");
			if($query->num_rows()>0){
				foreach ($query->result() as $row) {
	            	$data []= array(
	            				"nip"=>$row->nip,
	            				"bobot"=>$row->bobot,
	            				"id_sesi"=>$id_sesi
	            			);
	            }
	        }else{
	        		$data []= array(
	            				"nip"=>'',
	            				"bobot"=>'',
	            				"id_sesi"=>''
	            			);
	        }

			$get_name_kategori = $this->db->query("select a.kategori as kat from kategori_ujian a join sesi b on b.id_kategori_ujian=a.id where b.id_sesi='$id_sesi' ");
			$dt_row = $get_name_kategori->row();
			if($dt_row->kat=="PAPI")
			{
				$data_pegawai = $this->db->query("select distinct(nip) from jawaban where id_sesi='$id_sesi' ");
				$dt_hasil = array();
				foreach ($data_pegawai->result() as $key) {
					foreach ($kategori as $row) {
						$get_count = $this->db->query("select count(jawaban) as nilai from jawaban where nip='$key->nip' and jawaban='$row' ");
						$row_dt = $get_count->row();
						$dt_hasil[$key->nip][] = array($row=>$row_dt->nilai); 
					}
				}
	            $this->slice->with("thead",$kategori);
	            $this->slice->with("data",$dt_hasil);
	            $this->slice->with("id_sesi",$id_sesi);
	            $this->slice->with("id_kategori_ujian",$kategori_ujian);
				$this->slice->with("title","Hasil Ujian PAPI");
				$this->slice->with("subtitle",_get_data_sesi($id_sesi)->nama_sesi);
	            view("hasil_details_table_papi");
			}else{
				$this->slice->with("data",$data);
	            $this->slice->with("id_sesi",$id_sesi);
	            $this->slice->with("id_kategori_ujian",$kategori_ujian);
				$this->slice->with("title","Hasil Ujian");
				$this->slice->with("subtitle",_get_data_sesi($id_sesi)->nama_sesi);
	            view("hasil_details_table");
			}

			
            
		}

		public function cetak($kategori_ujian,$id_sesi)
		{
		
			$kategori = array("A","B","C","D","E","F","G","I","K","L","N","O","P","R","S","T","V","W","X","Z");
			$query = $this->db->query("select c.nip,sum(a.bobot) as bobot , e.jenis_soal, d.id_sesi from soal_dinas a join jawaban b on a.id=b.id_soal join pegawai c on c.nip=b.nip join sesi d on c.id_sesi=d.id_sesi join jenis_soal e on a.id_jenis_soal=e.id join manajemen_ujian f on f.id_jenis_soal=e.id where b.id_sesi ='$id_sesi' and d.id_kategori_ujian='$kategori_ujian' group by e.id,c.nip");
			if($query->num_rows()>0){
				foreach ($query->result() as $row) {
	            	$data []= array(
	            				"nip"=>$row->nip,
	            				"bobot"=>$row->bobot,
	            				"id_sesi"=>$id_sesi
	            			);
	            }
	        }else{
	        		$data []= array(
	            				"nip"=>'',
	            				"bobot"=>'',
	            				"id_sesi"=>''
	            			);
	        }

			$get_name_kategori = $this->db->query("select a.kategori as kat from kategori_ujian a join sesi b on b.id_kategori_ujian=a.id where b.id_sesi='$id_sesi' ");
			$dt_row = $get_name_kategori->row();
			if($dt_row->kat=="PAPI")
			{
				$data_pegawai = $this->db->query("select distinct(nip) from jawaban where id_sesi='$id_sesi' ");
				$dt_hasil = array();
				foreach ($data_pegawai->result() as $key) {
					foreach ($kategori as $row) {
						$get_count = $this->db->query("select count(jawaban) as nilai from jawaban where nip='$key->nip' and jawaban='$row' ");
						$row_dt = $get_count->row();
						$dt_hasil[$key->nip][] = array($row=>$row_dt->nilai); 
					}
				}
	            $this->slice->with("thead",$kategori);
	            $this->slice->with("data",$dt_hasil);
	            $this->slice->with("id_sesi",$id_sesi);
	            $this->slice->with("id_kategori_ujian",$kategori_ujian);
	            $this->slice->with("subtitle",_get_data_sesi($id_sesi)->nama_sesi);
				$this->slice->with("title","Hasil Ujian PAPI");
	            view("excel_papi");
			}else{
				$this->slice->with("data",$data);
	            $this->slice->with("id_sesi",$id_sesi);
	            $this->slice->with("id_kategori_ujian",$kategori_ujian);
	            $this->slice->with("subtitle",_get_data_sesi($id_sesi)->nama_sesi);
				$this->slice->with("title","Hasil Ujian");
	            view("excel");
			}
		}




	}
?>