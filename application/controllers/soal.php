<?php

	class soal extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
			$this->load->library('excel');
		}

		public function index()
		{
				$this->db->select('soal_dinas.*,jenis_soal.jenis_soal');
				$this->db->from('soal_dinas');
				$this->db->join('jenis_soal', 'jenis_soal.id = soal_dinas.id_jenis_soal');
				$this->db->order_by('soal_dinas.id', 'ASC');
				$query = $this->db->get();
				$this->slice->with("data",$query->result());
				$this->slice->with("title","Soal Ujian");
				view("soal_view");
		}

		public function papi_add()
		{
			$this->slice->with("message","");
			view("soal_papi_add");
		}

		public function page_add()
		{
			$this->slice->with("message","");
			view("soal_add");
		}

		public function simpan_soal()
		{
			$check_set = $this->input->post("check_set");
			$bidang_soal = $this->input->post("bidang_soal");
			if($check_set=='papi')
			{

				$this->form_validation->set_rules('jawaban_a','Soal A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_b','Soal B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_a','Jawaban A','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_b','Jawaban B','required',  array('required' => '%s Tidak boleh kosong'));

				if($this->form_validation->run() != false){
					
						
						$data = array(
							'soal'=>"",
							'jawaban_a'=>$this->input->post('jawaban_a'),
							'jawaban_b'=>$this->input->post('jawaban_b'),
							'bobot_a'=>$this->input->post('bobot_a'),
							'bobot_b'=>$this->input->post('bobot_b'),
							'status'=>$check_set,
							'id_jenis_soal'=>$bidang_soal
						);
						$insert = $this->db->insert("soal_dinas",$data);

					
					if($insert)
					{
						redirect("soal");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("soal_papi_add");
					}
				}else{
					$error_message = validation_errors();
					$this->slice->with("message",$error_message);
					view("soal_papi_add");
				}
			}
			elseif($check_set=='single')
			{
				
				$this->form_validation->set_rules('soal','Soal','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_a','Jawaban A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_b','Jawaban B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_c','Jawaban C','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_d','Jawaban D','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_e','Jawaban E','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('kunci_jawaban','Kunci Jawaban','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot','Bobot','required',  array('required' => '%s Tidak boleh kosong'));

				if($this->form_validation->run() != false){
					
						
						$data = array(
							'soal'=>$this->input->post('soal'),
							'jawaban_a'=>$this->input->post('jawaban_a'),
							'jawaban_b'=>$this->input->post('jawaban_b'),
							'jawaban_c'=>$this->input->post('jawaban_c'),
							'jawaban_d'=>$this->input->post('jawaban_d'),
							'jawaban_e'=>$this->input->post('jawaban_e'),
							'kunci_jawaban'=>$this->input->post('kunci_jawaban'),
							'bobot'=>$this->input->post('bobot'),
							'status'=>$check_set,
							'id_jenis_soal'=>$bidang_soal
						);
						$insert = $this->db->insert("soal_dinas",$data);

					
					if($insert)
					{
						redirect("soal");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("soal_add");
					}
				}else{
					$error_message = validation_errors();
					$this->slice->with("message",$error_message);
					view("soal_add");
				}

			}else{
				$this->form_validation->set_rules('soal','Soal','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_a','Jawaban A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_b','Jawaban B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_c','Jawaban C','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_d','Jawaban D','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_e','Jawaban E','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_a','Jawaban A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_b','Jawaban B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_c','Jawaban C','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_d','Jawaban D','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_e','Jawaban E','required',  array('required' => '%s Tidak boleh kosong'));

				if($this->form_validation->run() != false){
					
						$data = array(
							'soal'=>$this->input->post('soal'),
							'jawaban_a'=>$this->input->post('jawaban_a'),
							'jawaban_b'=>$this->input->post('jawaban_b'),
							'jawaban_c'=>$this->input->post('jawaban_c'),
							'jawaban_d'=>$this->input->post('jawaban_d'),
							'jawaban_e'=>$this->input->post('jawaban_e'),
							'bobot_a'=>$this->input->post('bobot_a'),
							'bobot_b'=>$this->input->post('bobot_b'),
							'bobot_c'=>$this->input->post('bobot_c'),
							'bobot_d'=>$this->input->post('bobot_d'),
							'bobot_e'=>$this->input->post('bobot_e'),
							'status'=>$check_set,
							'id_jenis_soal'=>$bidang_soal
						);
						$insert = $this->db->insert("soal_dinas",$data);

					
					if($insert)
					{
						redirect("soal");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("soal_add");
					}
				}else{
					$error_message = validation_errors();
					$this->slice->with("message",$error_message);
					view("soal_add");
				}
			}

	 
			

			
		}

		public function ubah_soal($id)
		{
			$query = $this->db->get_where('soal_dinas', array('id' => $id));
			$this->slice->with("data",$query->row());
			$this->slice->with("message","");
			if($query->row()->status!="papi")
			{
				view("soal_edit");	
			}else{
				view("soal_papi_edit");
			}
			
		}

		public function ubah()
		{

			$check_set = $this->input->post("check_set");
			$bidang_soal = $this->input->post("bidang_soal");
			if($check_set=='papi')
			{

				$this->form_validation->set_rules('jawaban_a','Soal A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_b','Soal B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_a','Jawaban A','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_b','Jawaban B','required',  array('required' => '%s Tidak boleh kosong'));
				$id = $this->input->post("id");
				if($this->form_validation->run() != false){
					
						
						$data = array(
							'soal'=>"",
							'jawaban_a'=>$this->input->post('jawaban_a'),
							'jawaban_b'=>$this->input->post('jawaban_b'),
							'bobot_a'=>$this->input->post('bobot_a'),
							'bobot_b'=>$this->input->post('bobot_b'),
							'status'=>$check_set,
							'id_jenis_soal'=>$bidang_soal
						);
						$this->db->where('id', $id);
						$update = $this->db->update("soal_dinas",$data);

						
					if($update)
					{
						redirect("soal");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("soal_papi_add");
					}
				}else{
					$error_message = validation_errors();
					$this->slice->with("message",$error_message);
					view("soal_papi_add");
				}
			}
			elseif($check_set=='single')
			{
				
				$this->form_validation->set_rules('soal','Soal','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_a','Jawaban A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_b','Jawaban B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_c','Jawaban C','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_d','Jawaban D','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_e','Jawaban E','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('kunci_jawaban','Kunci Jawaban','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot','Bobot','required',  array('required' => '%s Tidak boleh kosong'));

				$id = $this->input->post("id");
				if($this->form_validation->run() != false){
					
						
						$data = array(
							'soal'=>$this->input->post('soal'),
							'jawaban_a'=>$this->input->post('jawaban_a'),
							'jawaban_b'=>$this->input->post('jawaban_b'),
							'jawaban_c'=>$this->input->post('jawaban_c'),
							'jawaban_d'=>$this->input->post('jawaban_d'),
							'jawaban_e'=>$this->input->post('jawaban_e'),
							'kunci_jawaban'=>$this->input->post('kunci_jawaban'),
							'bobot'=>$this->input->post('bobot'),
							'status'=>$check_set,
							'id_jenis_soal'=>$bidang_soal
						);
						$this->db->where('id', $id);
						$update = $this->db->update("soal_dinas",$data);

					
					if($update)
					{
						redirect("soal");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("soal_add");
					}
				}else{
					$error_message = validation_errors();
					$this->slice->with("message",$error_message);
					view("soal_add");
				}

			}else{
				$this->form_validation->set_rules('soal','Soal','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_a','Jawaban A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_b','Jawaban B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_c','Jawaban C','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_d','Jawaban D','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('jawaban_e','Jawaban E','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_a','Jawaban A','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_b','Jawaban B','required' ,  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_c','Jawaban C','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_d','Jawaban D','required',  array('required' => '%s Tidak boleh kosong'));
				$this->form_validation->set_rules('bobot_e','Jawaban E','required',  array('required' => '%s Tidak boleh kosong'));
				$id = $this->input->post("id");
				if($this->form_validation->run() != false){
					
						$data = array(
							'soal'=>$this->input->post('soal'),
							'jawaban_a'=>$this->input->post('jawaban_a'),
							'jawaban_b'=>$this->input->post('jawaban_b'),
							'jawaban_c'=>$this->input->post('jawaban_c'),
							'jawaban_d'=>$this->input->post('jawaban_d'),
							'jawaban_e'=>$this->input->post('jawaban_e'),
							'bobot_a'=>$this->input->post('bobot_a'),
							'bobot_b'=>$this->input->post('bobot_b'),
							'bobot_c'=>$this->input->post('bobot_c'),
							'bobot_d'=>$this->input->post('bobot_d'),
							'bobot_e'=>$this->input->post('bobot_e'),
							'status'=>$check_set,
							'id_jenis_soal'=>$bidang_soal
						);
						$this->db->where('id', $id);
						$update = $this->db->update("soal_dinas",$data);

					
					if($update)
					{
						redirect("soal");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("soal_add");
					}
				}else{
					$error_message = validation_errors();
					$this->slice->with("message",$error_message);
					view("soal_add");
				}
			}
		}

		public function hapus_soal($id)
		{
			$query = $this->db->query("delete from soal_dinas where id='$id' ");
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}

		public function search_by()
		{
			$id_jenis_soal = $this->input->post("kode_soal");
			if(!isset($id_jenis_soal))
			{
				$this->db->select('soal_dinas.*,jenis_soal.jenis_soal');
				$this->db->from('soal_dinas');
				$this->db->join('jenis_soal', 'jenis_soal.id = soal_dinas.id_jenis_soal');
				$this->db->order_by('soal_dinas.id', 'ASC');
				$query = $this->db->get();
				$this->slice->with("data",$query->result());
				$this->slice->with("title","Soal Ujian");
				view("soal_view");
			}else{
				
				$this->db->select('soal_dinas.*,jenis_soal.jenis_soal');
				$this->db->from('soal_dinas');
				$this->db->join('jenis_soal', 'jenis_soal.id = soal_dinas.id_jenis_soal');
				$this->db->where('id_jenis_soal',$id_jenis_soal);
				$this->db->order_by('soal_dinas.id', 'ASC');
				$query = $this->db->get();
				$this->slice->with("data",$query->result());
				$this->slice->with("title","Soal Ujian");
				view("soal_view");
			}
		}

		public function import_csv()
		{

			  if(isset($_FILES["csv_file"]["name"]))
			{
				$path = $_FILES["csv_file"]["tmp_name"];
				$object = PHPExcel_IOFactory::load($path);
				foreach($object->getWorksheetIterator() as $worksheet)
				{
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for($row=2; $row<=$highestRow; $row++)
					{
						$soal = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
						$jawaban_a = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
						$bobot_a = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
						$jawaban_b = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
						$bobot_b = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
						$jawaban_c = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
						$bobot_c = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
						$jawaban_d = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
						$bobot_d = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
						$jawaban_e = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
						$bobot_e = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
						$kunci_jawaban = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
						$id_jenis_soal = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
						$bobot = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
						$status = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
						$data[] = array(
					          'soal'  => $soal,
					          'jawaban_a'   => $jawaban_a,
					          'bobot_a'   => $bobot_a,
					          'jawaban_b'   => $jawaban_b,
					          'bobot_b'   => $bobot_b,
					          'jawaban_c'   => $jawaban_c,
					          'bobot_c'   => $bobot_c,
					          'jawaban_d'   => $jawaban_d,
					          'bobot_d'   => $bobot_d,
					          'jawaban_e'   => $jawaban_e,
					          'bobot_e'   => $bobot_e,
					          'kunci_jawaban'   => $kunci_jawaban,
					          'id_jenis_soal'   => $id_jenis_soal,
					          'bobot'   => $bobot,
					          'status' => $status
						);
					}
				}
				$query = $this->db->insert_batch('soal_dinas', $data);
				if($query)
				  {
				   		redirect("soal");
				  }else
			  	{
			   		$error_message = $this->db->error();
				 	$this->slice->with("message",$error_message);
				 	view("soal_add");
			 	 }
			}	
		}

		 public function export(){
		   
		    $excel = new PHPExcel();
		    // Settingan awal fil excel
		    $excel->getProperties()->setCreator('My Notes Code')
		                 ->setLastModifiedBy('My Notes Code')
		                 ->setTitle("Data Siswa")
		                 ->setSubject("Siswa")
		                 ->setDescription("Laporan Semua Data Siswa")
		                 ->setKeywords("Data Siswa");
		    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		    $style_col = array(
		      'font' => array('bold' => true), // Set font nya jadi bold
		      'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
		        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
		      ),
		      'borders' => array(
		        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
		        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
		        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
		        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		      )
		    );
		    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		    $style_row = array(
		      'alignment' => array(
		        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
		      ),
		      'borders' => array(
		        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
		        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
		        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
		        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		      )
		    );
		    $excel->setActiveSheetIndex(0)->setCellValue('A1', "Soal Ujian"); // Set kolom A1 dengan tulisan "DATA SISWA"
		    $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
		    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		    $excel->getActiveSheet()->getStyle('B')->getAlignment()->setWrapText(true); 
		    $excel->getActiveSheet()->getStyle('C')->getAlignment()->setWrapText(true); 
		    $excel->getActiveSheet()->getStyle('E')->getAlignment()->setWrapText(true); 
		    $excel->getActiveSheet()->getStyle('G')->getAlignment()->setWrapText(true); 
		    $excel->getActiveSheet()->getStyle('I')->getAlignment()->setWrapText(true); 
		    $excel->getActiveSheet()->getStyle('K')->getAlignment()->setWrapText(true); 
		    // Buat header tabel nya pada baris ke 3
		    $excel->setActiveSheetIndex(0)->setCellValue('A3', "id"); // Set kolom A3 dengan tulisan "NO"
		    $excel->setActiveSheetIndex(0)->setCellValue('B3', "soal"); // Set kolom B3 dengan tulisan "NIS"
		    $excel->setActiveSheetIndex(0)->setCellValue('C3', "jawaban a"); // Set kolom C3 dengan tulisan "NAMA"
		    $excel->setActiveSheetIndex(0)->setCellValue('D3', "bobot a"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
		    $excel->setActiveSheetIndex(0)->setCellValue('E3', "jawaban b"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('F3', "bobot b"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('G3', "jawban c"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('H3', "bobot c"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('I3', "jawaban d"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('J3', "bobot d"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('K3', "jawaban e"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('L3', "bobot e"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('M3', "kunci jawaban"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('N3', "id jenis soal"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('O3', "bobot"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('P3', "status"); // Set kolom E3 dengan tulisan "ALAMAT"
		    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
		    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
		    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		    $siswa = $this->db->get("soal_dinas");
		    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
		    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		    foreach($siswa->result() as $data){ // Lakukan looping pada variabel siswa
		      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $data->id);
		      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->soal);
		      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->jawaban_a);
		      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->bobot_a);
		      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->jawaban_b);
		      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->bobot_b);
		      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->jawaban_c);
		      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->bobot_c);
		      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->jawaban_d);
		      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->bobot_d);
		      $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data->jawaban_e);
		      $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $data->bobot_e);
		      $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $data->kunci_jawaban);
		      $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $data->id_jenis_soal);
		      $excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $data->bobot);
		      $excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, $data->status);
		      
		      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
		      
		      $no++; // Tambah 1 setiap kali looping
		      $numrow++; // Tambah 1 setiap kali looping
		    }
		    // Set width kolom
		    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
		    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
		    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
		    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(5); // Set width kolom D
		    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25); // Set width kolom E
		    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(5); // Set width kolom A
		    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(25); // Set width kolom B
		    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(5); // Set width kolom C
		    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(25); // Set width kolom D
		    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(5); // Set width kolom E
		    $excel->getActiveSheet()->getColumnDimension('K')->setWidth(25); // Set width kolom A
		    $excel->getActiveSheet()->getColumnDimension('L')->setWidth(5); // Set width kolom B
		    $excel->getActiveSheet()->getColumnDimension('M')->setWidth(5); // Set width kolom C
		    $excel->getActiveSheet()->getColumnDimension('N')->setWidth(5); // Set width kolom D
		    $excel->getActiveSheet()->getColumnDimension('O')->setWidth(5); // Set width kolom E
		    $excel->getActiveSheet()->getColumnDimension('P')->setWidth(5); // Set width kolom E
		    
		    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		    // Set orientasi kertas jadi LANDSCAPE
		    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		    // Set judul file excel nya
		    $excel->getActiveSheet(0)->setTitle("Soal Ujian");
		    $excel->setActiveSheetIndex(0);
		    // Proses file excel
		    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		    header('Content-Disposition: attachment; filename="Data Siswa.xlsx"'); // Set nama file excel nya
		    header('Cache-Control: max-age=0');
		    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		    $write->save('php://output');
		    view("soal_view");
		  }


	}


?>