<?php
	
	class manajemen_ujian extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
		}

		public function index()
		{
			$this->db->select('manajemen_ujian.id as id_manajemen ,jenis_soal.jenis_soal as jenis_soal, kategori_ujian.kategori as kategori, manajemen_ujian.persentase as total_nilai, manajemen_ujian.passing_grade');
			$this->db->from('manajemen_ujian');
			$this->db->join('kategori_ujian', 'kategori_ujian.id = manajemen_ujian.id_kategori_ujian');
			$this->db->join('jenis_soal', 'jenis_soal.id = manajemen_ujian.id_jenis_soal');
			$this->db->order_by('manajemen_ujian.id', 'ASC');
			$query = $this->db->get();
			$this->slice->with("message","");
			$this->slice->with("title","Manajemen Soal Ujian");
			$this->slice->with("data",$query->result());
			view("manajemen_ujian");
		}

		public function view_form()
		{
			$this->slice->with("message","");
			view("form_manajemen_ujian");
		}

		public function insert()
		{
			$this->form_validation->set_rules('persentase','Jumlah Soal','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('passing_grade','Passing Grade','required',  array('required' => '%s Tidak boleh kosong'));
	 		
			if($this->form_validation->run() != false){
				$kode_kategori = $this->input->post('kode_kategori');
				$get_total_soal = $this->db->query("select * from kategori_ujian where id='$kode_kategori' ");
				$total = $get_total_soal->row();
				if($total->jlh_soal>=sum_jlh_soal($kode_kategori,$this->input->post("persentase")) ){

					$data = array(
						'id_kategori_ujian'=>$this->input->post("kode_kategori"),
						'id_jenis_soal'=>$this->input->post("kode_soal"),
						'persentase'=>$this->input->post("persentase"),
						'passing_grade'=>$this->input->post("passing_grade"),
					);
					$insert = $this->db->insert("manajemen_ujian",$data);
					if($insert)
					{
						redirect("manajemen_ujian");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("form_manajemen_ujian");
					}
				}else{

						$this->slice->with("message","Maaf, jumlah yang diinput melebihi batas soal ujian");
						view("form_manajemen_ujian");
				}
				
				
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				view("form_manajemen_ujian");
			}
		}

		public function view_edit($id)
		{
			$this->db->select('manajemen_ujian.id as id_manajemen ,jenis_soal.jenis_soal as jenis_soal, kategori_ujian.kategori as kategori, manajemen_ujian.persentase as total_nilai , kategori_ujian.id as id_kategori_ujian , jenis_soal.id as id_jenis_soal , manajemen_ujian.passing_grade');
			$this->db->from('manajemen_ujian');
			$this->db->join('kategori_ujian', 'kategori_ujian.id = manajemen_ujian.id_kategori_ujian');
			$this->db->join('jenis_soal', 'jenis_soal.id = manajemen_ujian.id_jenis_soal');
			$this->db->order_by('manajemen_ujian.id', 'ASC');
			$this->db->where("manajemen_ujian.id",$id);
			$query = $this->db->get();
			$this->slice->with("message","");
			$this->slice->with("title","Manajemen Soal Ujian");
			$this->slice->with("data",$query->row());
			view("manajemen_ujian_edit");
		}

		public function update()
		{
			$this->form_validation->set_rules('persentase','Jumlah Soal','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('passing_grade','Passing Grade','required',  array('required' => '%s Tidak boleh kosong'));
	 		$id = $this->input->post("id");
			if($this->form_validation->run() != false){
				$kode_kategori = $this->input->post('kode_kategori');
				$get_total_soal = $this->db->query("select * from kategori_ujian where id='$kode_kategori' ");
				$total = $get_total_soal->row();
				$hasil_akhir = 0;
				
				
			
					$hasil = $this->input->post("persentase")-check_after_before_update($kode_kategori);
					$hasil_akhir = check_after_before_update($kode_kategori)+$hasil;
					
		
			
					$data=array('id_kategori_ujian'=>$this->input->post("kode_kategori"),
						'id_jenis_soal'=>$this->input->post("kode_soal"),
						'persentase'=>$this->input->post("persentase"),
						'passing_grade'=>$this->input->post("passing_grade"),
					);
					$this->db->where('id', $id);
					$update = $this->db->update("manajemen_ujian",$data);
					if($update)
					{
						redirect("manajemen_ujian");
					}else{
						$error_message = $this->db->error();
						$this->slice->with("message",$error_message);
						view("manajemen_ujian_edit");
					}
				
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				$this->db->select('*');
				$this->db->from('manajemen_ujian');
				$this->db->join('kategori_ujian', 'kategori_ujian.id = manajemen_ujian.id_kategori_ujian');
				$this->db->join('jenis_soal', 'jenis_soal.id = manajemen_ujian.id_jenis_soal');
				$this->db->order_by('manajemen_ujian.id', 'ASC');
				$this->db->where("manajemen_ujian.id",$id);
				$query = $this->db->get();
				$this->slice->with("title","Manajemen Soal Ujian");
				$this->slice->with("data",$query->row());
				view("manajemen_ujian_edit");
			 }
		}

		public function delete($id)
		{
			$query = $this->db->query("delete from manajemen_ujian where id='$id' ");
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}
	}

?>