<?php

	class User extends CI_Controller
	{
		public function __construct(){
			parent::__construct();
			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
		}

		public function index()
		{
            view("user");

        }
	}
?>