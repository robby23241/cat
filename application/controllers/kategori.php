<?php
	
	class kategori extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
		}

		public function index()
		{
			$query = $this->db->get("kategori_ujian");
			$this->slice->with("message","");
			$this->slice->with("title","Kategori Ujian");
			$this->slice->with("data",$query->result());
			view("kategori_view");
		}

		public function view_form()
		{
			$this->slice->with("message","");
			view("kategori_add");
		}

		public function insert()
		{
			$this->form_validation->set_rules('kategori_ujian','Kategori Ujian','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('waktu','Waktu Pengerjaan Ujian','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('jlh_soal','Jumlah Soal','required',  array('required' => '%s Tidak boleh kosong'));
	 
			if($this->form_validation->run() != false){
				$data = array(
					'kategori'=>$this->input->post("kategori_ujian"),
					'waktu_pengerjaan'=>$this->input->post("waktu"),
					'jlh_soal'=>$this->input->post("jlh_soal")
				);
				$insert = $this->db->insert("kategori_ujian",$data);
				if($insert)
				{
					redirect("kategori");
				}else{
					$error_message = $this->db->error();
					$this->slice->with("message",$error_message);
					view("kategori_add");
				}
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				view("kategori_add");
			}

		}

		public function view_edit($id)
		{
			$query = $this->db->get_where('kategori_ujian', array('id' => $id));
			$this->slice->with("data",$query->row());
			$this->slice->with("message","");
			view("kategori_edit");
		}

		public function update()
		{
			$this->form_validation->set_rules('kategori_ujian','Kategori Ujian','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('waktu','Waktu Pengerjaan Ujian','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('jlh_soal','Jumlah Soal','required',  array('required' => '%s Tidak boleh kosong'));
	 		
	 		$id = $this->input->post("id");
			if($this->form_validation->run() != false){
				$data = array(
					'kategori'=>$this->input->post("kategori_ujian"),
					'waktu_pengerjaan'=>$this->input->post("waktu"),
					'jlh_soal'=>$this->input->post("jlh_soal")
				);
				$this->db->where('id', $id);
				$update = $this->db->update("kategori_ujian",$data);
				if($update)
				{
					redirect("kategori");
				}else{
					$error_message = $this->db->error();
					$this->slice->with("message",$error_message);
					view("kategori_add");
				}
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				view("kategori_add");
			}
		}

		public function delete($id)
		{
			$query = $this->db->query("delete from kategori_ujian where id='$id' ");
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}
	}

?>