<?php
	

    

	class pegawai extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
			$this->load->library('excel');
		}

		public function index()
		{
			$query = $this->db->query('select a.*,b.*,c.* from pegawai a join sesi b on a.id_sesi=b.id_sesi join kategori_ujian c on b.id_kategori_ujian=c.id');
			$this->slice->with("message","");
			$this->slice->with("title","Data Pegawai");
			$this->slice->with("data",$query->result());
			view("pegawai_view");
		}

		public function form_pegawai()
		{
			$this->slice->with("message","");
			view("pegawai_form");
		}

		public function insert()
		{
			$this->form_validation->set_rules('nip','Nip Pegawai','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required',  array('required' => '%s Tidak boleh kosong'));
	 
			if($this->form_validation->run() != false){
				$data = array(
					'nip'=>$this->input->post("nip"),
					'nama_lengkap'=>$this->input->post("nama_lengkap"),
					'id_sesi'=>$this->input->post("id_sesi"),
					'password'=>12345
				);
				$insert = $this->db->insert("pegawai",$data);
				if($insert)
				{
					redirect("pegawai");
				}else{
					$error_message = $this->db->error();
					$this->slice->with("message",$error_message);
					view("pegawai_form");
				}
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				view("pegawai_form");
			}
		}

		public function view_edit($id)
		{
			$query = $this->db->get_where('pegawai', array('nip' => $id));
			$this->slice->with("data",$query->row());
			$this->slice->with("message","");
			view("pegawai_edit");
		}

		public function update()
		{
			$this->form_validation->set_rules('nip','Nip Pegawai','required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required',  array('required' => '%s Tidak boleh kosong'));
	 		
	 		$id = $this->input->post("nip_old");
			if($this->form_validation->run() != false){
				$data = array(
					'nip'=>$this->input->post("nip"),
					'nama_lengkap'=>$this->input->post("nama_lengkap"),
					'id_sesi'=>$this->input->post("id_sesi")
				);
				$this->db->where('nip', $id);
				$update = $this->db->update("pegawai",$data);
				if($update)
				{
					redirect("pegawai");
				}else{
					$error_message = $this->db->error();
					$this->slice->with("message",$error_message);
					view("pegawai_edit");
				}
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				view("pegawai_edit");
			}
		}

		public function delete($id)
		{	
			$query = $this->db->query("delete from pegawai where nip='$id' ");
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}

		public function import_csv()
		{
			
			if(isset($_FILES["csv_file"]["name"]))
			{
				$path = $_FILES["csv_file"]["tmp_name"];
				$object = PHPExcel_IOFactory::load($path);
				foreach($object->getWorksheetIterator() as $worksheet)
				{
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for($row=2; $row<=$highestRow; $row++)
					{
						$nip = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
						$nama_lengkap = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
						$id_sesi = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
						$data[] = array(
							'nip'		=>	$nip,
							'nama_lengkap'			=>	$nama_lengkap,
							'id_sesi'				=>	$id_sesi
						);
					}
				}
				$query = $this->db->insert_batch('pegawai', $data);
				if($query)
				  {
				   		redirect("pegawai");
				  }else
			  	{
			   		$error_message = $this->db->error();
				 	$this->slice->with("message",$error_message);
				 	view("pegawai_form");
			 	 }
			}	
		}

		public function get_sesi($id){
	        $data = $this->db->query("select * from sesi where id_kategori_ujian='$id' ");
	        // get_where('sesi', array('id_kategori_ujian' =>$id));
	        echo json_encode($data->result());
	    }
	}
?>