<?php
	
	class jenis_soal extends CI_Controller
	{
		public function __construct(){
			parent::__construct();
			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
		}

		public function index()
		{
			$query = $this->db->get("jenis_soal");
			$this->slice->with("message","");
			$this->slice->with("title","Jenis Soal Ujian");
			$this->slice->with("data",$query->result());
			view("jenis_soal_view");
		}

		public function view_form()
		{
			$this->slice->with("message","");
			view("jenis_soal_add");
		}

		public function insert()
		{
			$this->form_validation->set_rules('jenis_soal','Jenis Soal','required',  array('required' => '%s Tidak boleh kosong'));
	 
			if($this->form_validation->run() != false){
				$data = array(
					'jenis_soal'=>$this->input->post("jenis_soal")
				);
				$insert = $this->db->insert("jenis_soal",$data);
				if($insert)
				{
					redirect("jenis_soal");
				}else{
					$error_message = $this->db->error();
					$this->slice->with("message",$error_message);
					view("jenis_soal_add");
				}
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				view("jenis_soal_add");
			}

		}

		public function view_edit($id)
		{
			$query = $this->db->get_where('jenis_soal', array('id' => $id));
			$this->slice->with("data",$query->row());
			$this->slice->with("message","");
			view("jenis_soal_edit");
		}

		public function update()
		{
			$this->form_validation->set_rules('jenis_soal','Jenis Soal','required',  array('required' => '%s Tidak boleh kosong'));
	 		
	 		$id = $this->input->post("id");
			if($this->form_validation->run() != false){
				$data = array(
					'jenis_soal'=>$this->input->post("jenis_soal")
				);
				$this->db->where('id', $id);
				$update = $this->db->update("jenis_soal",$data);
				if($update)
				{
					redirect("jenis_soal");
				}else{
					$error_message = $this->db->error();
					$this->slice->with("message",$error_message);
					view("jenis_soal_add");
				}
			}else{
				$error_message = validation_errors();
				$this->slice->with("message",$error_message);
				view("jenis_soal_add");
			}
		}

		public function delete($id)
		{
			$query = $this->db->query("delete from jenis_soal where id='$id' ");
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}
	}