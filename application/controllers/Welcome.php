<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata("level")!=1)
		{
			redirect("login");
		}
	}

	public function index()
	{
		$query = $this->db->query("select count(a.id) as jumlah, b.jenis_soal as jenis_soal from soal_dinas a join jenis_soal b on a.id_jenis_soal =b.id GROUP BY a.id_jenis_soal");
		$total_pegawai = $this->db->query("select count(nip) as jumlah from pegawai");
		$row_peg = $total_pegawai->row();
		$this->slice->with("data",$query->result());
		$this->slice->with("datapeg",$row_peg->jumlah);


		view('dashboard');
	}
}
