<?php

	class Admin extends CI_Controller
	{
		public function __construct(){
			parent::__construct();
			if($this->session->userdata("level")!=1)
			{
				redirect("login");
			}
		}

		public function update_form()
		{
			$query = $this->db->get("login");
			$this->slice->with("message",'');
			$this->slice->with("data",$query->row());
			view('update_form');
		}

		public function update()
		{
			$data = array(
				'password'=>md5($this->input->post("password"))
			);

			$this->db->where("username",$this->input->post("username"));
			$update = $this->db->update("login",$data);
			if($update)
			{
				redirect("login/logout");
			}
		}

	}
?>