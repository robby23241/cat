<?php

	class tes extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			if($this->session->userdata("nip")==null)
			{
				$this->slice->with("message","");
				view("login_peserta");
			}else
			{
				// redirect("Soal_ujian/main_ujian");
				foreach($this->session->userdata("data") as $row)
	            {
	                $data [] = $row['id'];
	            }

	            if(empty($id_soal))
	            {
	                for($i=0;$i<count($data); $i++)
	                {
	                    if($i==0)
	                    {
	                        $query = $this->db->query("select * from soal_dinas where id ='$data[$i]' ");
	                        $first_question = $query->result();
	                    }
	                }
	            }else{
	                $query = $this->db->query("select * from soal_dinas where id ='$id_soal' ");
	                $first_question = $query->result();
	            }
	            $id_soals = 0;
	            foreach ($first_question as $row) {
	            	$id_soals = $row->id;
	            }
				redirect("Soal_ujian/main_ujian/".$id_soals);
			}
			
		}

		public function login_tes()
		{
			$nip = $this->input->post("nip");
			$nama = $this->input->post("nama");
		

			$where = array(
					'nip' => $nip,
					'password' => $nama
				);
			$query = $this->db->get_where('pegawai',$where);


				
					if($query->num_rows()>0)
					{

						$row = $query->row();
						$query_sesi = $this->db->query("select a.waktu,a.waktu_akhir,a.id_sesi,b.nama_lengkap from sesi a join pegawai b on a.id_sesi=b.id_sesi join kategori_ujian c on a.id_kategori_ujian=c.id where b.nip = '$nip' and a.waktu<=NOW() and a.waktu_akhir>=NOW() ");
						
						if($query_sesi->num_rows()>0)
						{
							$id_s = $query_sesi->row()->id_sesi;
							$check_ujian = $this->db->query("select * from hasil where nip='$nip' and id_sesi='$id_s' ");
							
							if($check_ujian->num_rows()>0)
							{
								$this->slice->with("message","Maaf anda sudah pernah mengikuti ujian ini ");
								view("page_check_ujian");
							}else{
								
							 	$id_kategori_ujian = _get_data_sesi($query_sesi->row()->id_sesi)->id_kategori_ujian;
							
								$data_session = array(
											'nip' => $nip,
											'nama' => $query_sesi->row()->nama_lengkap,
											'id_sesi'=>$query_sesi->row()->id_sesi,
											'kategori_ujian'=>$this->get_name_ujian($id_kategori_ujian)->kategori,
											'data'=>$this->get_soal(_get_data_sesi($query_sesi->row()->id_sesi)->id_kategori_ujian),
											'first'=>$this->get_first($id_kategori_ujian),
											'level'=>2
											);
								 
								$this->session->set_userdata($data_session);
								redirect("Soal_ujian");
							}
						}else{
							
							$this->slice->with("message","Maaf anda tidak berada pada sesi ini ");
							view("login_peserta");
						}
						
						
					}else{
						$this->slice->with("message","NIP dan password tidak sesuai");
						view("login_peserta");
					}
				
		}

		public function get_soal($id)
		{
            $get_jenis_soal = $this->db->query("select * from manajemen_ujian where id_kategori_ujian='$id' ");
            $data = array();

            $no = 1;
            foreach ($get_jenis_soal->result() as $rows) {
                $query = $this->db->query("SELECT * FROM soal_dinas where id_jenis_soal='$rows->id_jenis_soal' ORDER BY RAND ( )  LIMIT $rows->persentase  ");
                foreach ($query->result() as $row_soal) {
                   $data[] = array(
                            "no"=>$no,
                            "id"=>$row_soal->id,
                            "soal"=>$row_soal->soal,
                            "jawaban_a"=>$row_soal->jawaban_a,
                            "jawaban_b"=>$row_soal->jawaban_b,
                            "jawaban_c"=>$row_soal->jawaban_c,
                            "jawaban_d"=>$row_soal->jawaban_d,
                            "jawaban_e"=>$row_soal->jawaban_e,
                        );
                   $no++;
                }
                
            }

            return $data;
		}

		public function get_first($id)
        {

            $get_jenis_soal = $this->db->query("select * from manajemen_ujian where id_kategori_ujian='$id' LIMIT 1");
            $data = array();

            foreach ($get_jenis_soal->result() as $rows) {
                $query = $this->db->query("SELECT * FROM soal_dinas where id_jenis_soal='$rows->id_jenis_soal' ORDER BY RAND ( ) LIMIT 1  ");
                foreach ($query->result() as $row_soal) {
                   $data = array(
                            "id"=>$rows->id,
                            "soal"=>$row_soal->soal,
                            "jawaban_a"=>$row_soal->jawaban_a,
                            "jawaban_b"=>$row_soal->jawaban_b,
                            "jawaban_c"=>$row_soal->jawaban_c,
                            "jawaban_d"=>$row_soal->jawaban_d,
                            "jawaban_e"=>$row_soal->jawaban_e,
                            "id_jenis_soal"=>$rows->id_jenis_soal
                        );
                }
                
            }
            return $data;
        }

		public function logout_tes()
		{
			$this->session->sess_destroy();
			redirect('tes');
		}

		public function get_name_ujian($id)
		{
			$this->db->where("id",$id);
			$query = $this->db->get("kategori_ujian");

			return $query->row();
		}
	}
?>