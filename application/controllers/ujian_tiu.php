<?php

	class Ujian_tiu extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function ujian_tiu($id_soal="")
        {
             if($this->session->userdata("status")=="finish")
            {
                redirect("tes/logout_tes");
            }
            if($this->session->userdata('waktu_start'))
            {
                    $lewat = time() - $this->session->userdata('waktu_start');
            }else{
                    $this->session->set_userdata(array('waktu_start'=>time())); 
                    $lewat = 0;
            }


            
            $query = $this->db->query("select * from soal_tiu");
            foreach ($query->result() as $key ) {
                $data [] = $key->id;
            }
                
            

            if(empty($id_soal))
            {
                for($i=0;$i<count($data); $i++)
                {
                    if($i==0)
                    {
                        $query = $this->db->query("select * from soal_tiu where id ='$data[$i]' ");
                        $first_question = $query->result();
                    }
                }
            }else{
                $query = $this->db->query("select * from soal_tiu where id ='$id_soal' ");
                $first_question = $query->result();
            }
            $nip =$this->session->userdata('nip');
            $id_sesi = $this->session->userdata("id_sesi");
            $check_jawaban = $this->db->query("select * from jawaban_tiu where nip ='$nip' and id_soal='$id_soal' and id_sesi='$id_sesi' ");
            $row_check = $check_jawaban->row();
            if($check_jawaban->num_rows()>0)
            {
                $stat = $row_check->jawaban;
                $id = $row_check->id_jawaban;
                $fungsi = 'update';
            }else{
                $stat = '';
                $id = 0;
                $fungsi = 'simpan';
            }

            $all = $this->db->query("select * from soal_tiu ");
            $all_quest = $all->result();

            $this->slice->with("lewat",$lewat);
            $this->slice->with("all",$all_quest);
            $this->slice->with("id",$id);
            $this->slice->with("checked",$stat);
            $this->slice->with("fungsi",$fungsi);
            $this->slice->with("first",$first_question);
            $this->slice->with("another",get_tiu());
            view("soal_ujian_tiu_start");
        }


        public function simpan()
        {
           
            $nip = $this->session->userdata("nip");
            $id_sesi = $this->session->userdata("id_sesi");
            $id_soal = $this->input->post("id_soal");
            $answer = $this->input->post("answer");

            $data = array(
                "nip"=>$nip,
                "id_sesi"=>$id_sesi,
                "id_soal"=>$id_soal,
                "jawaban"=>$answer
            );
           
            $query = $this->db->insert("jawaban_tiu",$data);
            if($query){
               
                redirect("Ujian_tiu/ujian_tiu/".($id_soal+1) );

            }else
            {
            	var_dump("error");
            }
            

        }

        public function update()
        {
           
            $id = $this->input->post('id');
            $nip = $this->session->userdata("nip");
            $id_sesi = $this->session->userdata("id_sesi");
            $id_soal = $this->input->post("id_soal");
            $answer = $this->input->post("answer");

            $data = array(
                "nip"=>$nip,
                "id_sesi"=>$id_sesi,
                "id_soal"=>$id_soal,
                "jawaban"=>$answer
            );
            $this->db->where('id_jawaban', $id);
            $query = $this->db->update("jawaban_tiu",$data);
            if($query){
                
                redirect("Ujian_tiu/ujian_tiu/".($id_soal+1) );

            }
        }

	}

?>