<?php

	class login extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			view("login");
		}

		public function prosesLogin()
		{
			$this->form_validation->set_rules('username','Username','trim|required',  array('required' => '%s Tidak boleh kosong'));
			$this->form_validation->set_rules('password','Password','trim|required' ,  array('required' => '%s Tidak boleh kosong'));


			if($this->form_validation->run() != false){
				$username = $this->input->post("username");
				$password = $this->input->post("password");
				$where = array(
					'username' => $username,
					'password' => md5($password)
				);
				$query = $this->db->get_where('login',$where);
				if($query->num_rows()>0)
				{

					$data_session = array(
						'nama' => $username,
						'level' => 1
						);
			 
					$this->session->set_userdata($data_session);
			 
					redirect("welcome");
				}else{
					$this->slice->with("message","Gagal Login");
					view("login");
				}
			}else{
				$this->slice->with("message",validation_errors());
				view("login");
			}
		}

		public function logout()
		{
			$this->session->sess_destroy();
			redirect('login');
		}
	}

?>