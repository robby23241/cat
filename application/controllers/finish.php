<?php

	class finish extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			 if($this->session->userdata("level")!=2)
            {
                redirect("tes");
            }
		}

		public function index()
		{
			$nip = $this->session->userdata("nip");
			$query = $this->db->query("select a.*,b.* from jawaban a join soal_dinas b on a.id_soal=b.id where nip = '$nip' ");
			$row = $query->result();
			foreach ($row as $key ) {
				if($key->status=="single"){

					if($key->jawaban===$key->kunci_jawaban)
				 	{
				 		$bobot = $key->bobot;
				 		$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}else{
						$bobot = 0;
						$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}

					
				}else{
					if($key->jawaban==="A")
				 	{
				 		$bobot = $key->bobot_a;
				 		$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}else if($key->jawaban==="B"){
						$bobot = $key->bobot_b;
						$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}else if($key->jawaban==="C"){
						$bobot = $key->bobot_c;
						$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}else if($key->jawaban==="D"){
						$bobot = $key->bobot_d;
						$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}else if($key->jawaban==="E"){
						$bobot = $key->bobot_e;
						$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}
					else{
						$bobot = 0;
						$data [] = array("id_jenis_soal"=>$key->id_jenis_soal,'bobot'=>$bobot);
					}
				}
			}
			
			$this->slice->with("data",$data);
			view("soal_ujian_end");
		}
	}
?>