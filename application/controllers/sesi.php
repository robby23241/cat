<?php
	
	class sesi extends CI_Controller
	{
		public function __constrcut()
		{
			parent::__construct();
		}

		public function index()
		{
			$query = $this->db->query("select a.*, b.* from sesi a join kategori_ujian b on a.id_kategori_ujian=b.id");
			$this->slice->with("message","");
			$this->slice->with("title","Sesi Ujian");
			$this->slice->with("data",$query->result());
			view("sesi_view");
		}

		public function view_form()
		{
			$this->slice->with("message","");
			view("sesi_add");
		}

		public function insert()
		{
			$data = array(
				'nama_sesi'=>$this->input->post("nama_sesi"),
				'waktu'=>$this->input->post('waktu_mulai'),
				'waktu_akhir'=>$this->input->post('waktu_berakhir'),
				'id_kategori_ujian'=>$this->input->post('id_kategori_ujian')
			);

			$insert = $this->db->insert("sesi",$data);
			if($insert)
			{
				redirect("sesi");
			}else{
				$error_message = $this->db->error();
				$this->slice->with("message",$error_message);
					view("sesi_add");
			}
		}

		public function view_edit($id_sesi)
		{
			$query = $this->db->get_where('sesi', array('id_sesi' => $id_sesi));
			$this->slice->with("data",$query->row());
			$this->slice->with("message","");
			view("sesi_edit");
		}

		public function update()
		{
			
	 		$id = $this->input->post("id_sesi");
			$data = array(
				'nama_sesi'=>$this->input->post("nama_sesi"),
				'waktu'=>$this->input->post('waktu_mulai'),
				'waktu_akhir'=>$this->input->post('waktu_berakhir'),
				'id_kategori_ujian'=>$this->input->post('id_kategori_ujian')
			);
			$this->db->where('id_sesi', $id);
			$update = $this->db->update("sesi",$data);
			if($update)
			{
				redirect("sesi");
			}else{
				$error_message = $this->db->error();
				$this->slice->with("message",$error_message);
				view("sesi_add");
			}
			
		}

		public function delete($id)
		{
			$query = $this->db->query("delete from sesi where id_sesi='$id' ");
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}
	}


?>