<?php

	class Soal_ujian extends CI_Controller
	{

		public function __construct(){
			parent::__construct();
            if($this->session->userdata("level")!=2)
            {
                redirect("tes");
            }
		}

        public function get_soal_ujian_dinas($id)
        {
        	
        	$this->slice->with("data",$data);
        	
            view("soal_ujian");
        }

		public function index()
		{
             if($this->session->userdata("status")=="finish")
            {
                redirect("tes/logout_tes");
            }
			view("soal_ujian_start");
		}
        
		public function main_ujian($id_soal="")
		{
            if($this->session->userdata("status")=="finish")
            {
                redirect("tes/logout_tes");
            }
            if($this->session->userdata('waktu_start'))
            {
                    $lewat = time() - $this->session->userdata('waktu_start');
            }else{
                    $this->session->set_userdata(array('waktu_start'=>time())); 
                    $lewat = 0;
            }


            
            foreach($this->session->userdata("data") as $row)
            {
                $data [] = $row['id'];
            }

            if(empty($id_soal))
            {
                for($i=0;$i<count($data); $i++)
                {
                    if($i==0)
                    {
                        $query = $this->db->query("select * from soal_dinas where id ='$data[$i]' ");
                        $first_question = $query->result();
                    }
                }
            }else{
                $query = $this->db->query("select * from soal_dinas where id ='$id_soal' ");
                $first_question = $query->result();
            }
            $nip =$this->session->userdata('nip');
            $id_sesi = $this->session->userdata("id_sesi");
            $check_jawaban = $this->db->query("select * from jawaban where nip ='$nip' and id_soal='$id_soal' and id_sesi='$id_sesi' ");
            $row_check = $check_jawaban->row();
            if($check_jawaban->num_rows()>0)
            {
                $stat = $row_check->jawaban;
                $id = $row_check->id_jawaban;
                $fungsi = 'update';
            }else{
                $stat = '';
                $id = 0;
                $fungsi = 'simpan';
            }

            $this->slice->with("lewat",$lewat);
            $this->slice->with("id",$id);
            $this->slice->with("checked",$stat);
            $this->slice->with("fungsi",$fungsi);
            $this->slice->with("first",$first_question);
            $this->slice->with("another",get_soal());
            view("soal_ujian");
		}

        public function simpan()
        {
             foreach($this->session->userdata("data") as $row)
            {
                $data_soal [] = $row['id'];
            }

            
            foreach($this->session->userdata("data") as $row)
            {
                $data_id [$row['id']]= $row['no'];
            }

            foreach($this->session->userdata("data") as $row)
            {
                $data_soal_get [$row['no']]= $row['id'];
            }
           
            $nip = $this->session->userdata("nip");
            $id_sesi = $this->session->userdata("id_sesi");
            $id_soal = $this->input->post("id_soal");
            $answer = $this->input->post("answer");

            $data = array(
                "nip"=>$nip,
                "id_sesi"=>$id_sesi,
                "id_soal"=>$id_soal,
                "jawaban"=>$answer
            );
           
            $query = $this->db->insert("jawaban",$data);
            if($query){
                $this->session->set_userdata(array("next"=>$this->session->userdata("next")));
                redirect("Soal_ujian/main_ujian/".$data_soal_get[$data_id[$id_soal]+1] );

            }
            

        }

        public function update()
        {
            foreach($this->session->userdata("data") as $row)
            {
                $data_soal [] = $row['id'];
            }

            
            foreach($this->session->userdata("data") as $row)
            {
                $data_id [$row['id']]= $row['no'];
            }

            foreach($this->session->userdata("data") as $row)
            {
                $data_soal_get [$row['no']]= $row['id'];
            }
             $id = $this->input->post('id');
            $nip = $this->session->userdata("nip");
            $id_sesi = $this->session->userdata("id_sesi");
            $id_soal = $this->input->post("id_soal");
            $answer = $this->input->post("answer");

            $data = array(
                "nip"=>$nip,
                "id_sesi"=>$id_sesi,
                "id_soal"=>$id_soal,
                "jawaban"=>$answer
            );
            $this->db->where('id_jawaban', $id);
            $query = $this->db->update("jawaban",$data);
            if($query){
                $this->session->set_userdata(array("next"=>$this->session->userdata("next")+1));
                redirect("Soal_ujian/main_ujian/".$data_soal_get[$data_id[$id_soal]+1] );

            }
        }

		public function end_ujian()
		{
            $nip = $this->session->userdata("nip");
            $this->db->select('*');
            $this->db->from('soal_dinas');
            $this->db->join('jawaban', 'jawaban.id_soal = soal_dinas.id');
            $query = $this->db->get();
            $total = 0;
            foreach ($query->result() as $row) {
                if($row->kunci_jawaban==$row->jawaban)
                {
                    $total+=$row->bobot;
                }
            }
            $data = array(
                "nip"=>$nip,
                "id_sesi"=>$this->session->userdata("id_sesi"),
                "nilai"=>$total
            );
            $insert = $this->db->insert("hasil",$data);
            if($insert){
                $this->session->set_userdata(array('status'=>"finish"));
			    redirect("finish");
            }
		}
		public function result_ujian()
		{
			view("soal_ujian_result");
		}

	}
?>