@extends('main_base') 
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-12">
                    <form method="post" action="{{site_url()}}hasil/search_by_date">
                        <div class="row">
                            <div class="col-lg-12">
                                <span class="">Search</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 pt-1">
                                <input type="date" name="tgl_mulai" class="form-control">
                            </div>
                        </div>
                        <input type="submit" name="" value="search" class="btn btn-success mt-1">
                    </form>
                </div>
            </div>
            <div class="row pt-1">
                
                @foreach($data as $row)

                <div class="col-xl-3 col-lg-6 col-12">
                    <a href="{{site_url()}}hasil/details_pegawai/{{$row->id_kategori_ujian}}/{{$row->id_sesi}}">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media">
                                        <div class="media-body text-left w-100">
                                           <!--  <h3 class="primary">{{$row->total_pegawai}}</h3> -->
                                            <span>Soal {{$row->nama_sesi}}</span><br>
                                            <span>Soal {{$row->waktu}}</span>
                                        </div>
                                        <div class="media-right media-middle">
                                            <i class="icon-social-dropbox primary font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                @endforeach

            </div>
        </div>
    </div>
</div>
@endsection