@php
defined('BASEPATH') OR exit('No direct script access allowed');
@endphp


<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="CAT Prototype">
  <title>UJIAN CAT DISKOMINFO</title>  <!-- Change per each menu clicked? -->
  <link rel="apple-touch-icon" href="{{APP_ASSETS}}images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="{{APP_ASSETS}}images/ico/favicon.ico">
  <!-- Font Style start-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- Font style end -->
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/vendors.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/extensions/unslider.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/weather-icons/climacons.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}fonts/meteocons/style.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/charts/morris.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN Theme CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/app.css">
  <!-- END Theme CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}fonts/simple-line-icons/style.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}/css/core/colors/palette-gradient.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/pages/timeline.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}assets-plus//css/style.css">
  END Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/datatable/datatables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/responsive.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/colReorder.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/buttons.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/datatable/buttons.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/fixedHeader.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/sweetalert/sweetalert.css"/>

</head>

  @yield('content')

    <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <b>DISKOMINFO BINTAN</b>, All rights reserved.</span>
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/charts/raphael-min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/charts/morris.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="{{APP_ASSETS}}js/core/app-menu.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/core/app.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/scripts/customizer.js" type="text/javascript"></script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{APP_ASSETS}}js/scripts/pages/dashboard-ecommerce.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
  <script src="{{APP_ASSETS}}js/scripts/tables/datatables-extensions/datatable-responsive.js"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.colReorder.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.fixedHeader.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="{{APP_ASSETS}}vendors/sweetalert/sweetalert.min.js"></script>
  <script src="{{APP_ASSETS}}count_down/js/jquery.plugin.min.js"></script>
  <script src="{{APP_ASSETS}}count_down/js/jquery.countdown.js"></script>
</body>
</html>
  @section('javascript')
  @show
</body>
</html>