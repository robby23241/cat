<?php
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=$title.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
?>
<p><b>{{$title}} | {{$subtitle}}</b></p>
                    <table width="100%" border="1">
                      <thead>
                        @php $no = 1; @endphp

                        <tr>
                          <th>Nomor</th>
                          <th>Nip</th>
                          <th>Nama Lengkap</th>
                          @foreach($thead as $row)
                            <th>{{$row}}</th>
                          @endforeach
                        </tr>
                        
                      </thead>
                      <tbody>
                        @foreach(get_pegawai_sesi($id_sesi,$id_kategori_ujian) as $row)

                        
                              <tr>
                                <td>{{$no++}}</td>
                                <td>{{$row->nip}}</td>
                                <td>{{get_detail_pegawai($row->nip)->nama_lengkap}}</td>
                                @foreach($thead as $row_val)
                                  <td>{{get_jawaban_papi($row->nip,$row_val)}}</td>
                                @endforeach
                              </tr>
                         
                        @endforeach
                      </tbody>
                    </table>
                        