@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        <section id="constructor">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">{{$title}}</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a href="{{site_url()}}jenis_soal/view_form"><i class="ft-plus"></i></a></li>
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered dataex-res-constructor" id="data_soal">
                      <thead>
                        @php $no = 1; @endphp

                        <tr>
                          <th>Nomor</th>
                          <th>Id Soal</th>
                          <th>Jenis Soal</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                        
                      </thead>
                      <tbody>
                        @foreach($data as $row)
                            <tr>
                              <td>{{$no++}}</td>
                              <td>{{$row->id}}</td>
                              <td>{{$row->jenis_soal}}</td>
                              <td><a href="{{site_url()}}jenis_soal/view_edit/{{$row->id}}" class="btn btn-warning btn-sm">Edit</a></td>
                            <td><a href="javascript:void(0)" class="btn btn-danger btn-sm delete" data-id="{{$row->id}}">Delete</a></td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section("javascript")


<script type="text/javascript">
  
    $('#data_soal').dataTable({
            "searching": true,
            "ordering": true,
            "paging": "false"
        });
    
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#data_soal tbody').on('click', 'a.delete', function () {
        var id = $(this).data().id;
        swal({
            title: "Are you sure?",
            text: "Delete the file?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
          },
           function(isConfirm){
             if (isConfirm) {
              $.ajax({
                  url:"{{site_url()}}jenis_soal/delete/"+id,
                  type: "POST",
                  data: {id: id},
                  dataType: "html",
                  success: function (res) {
                    if(res.status==true){
                      swal("Done!","Delete success","success");
                      
                    }
                      location.reload();
                  }
              });
            }else{
                  swal("Cancelled", "Delete cancelled", "error");
            } 
         })

      });
    });
</script>
  @endsection

