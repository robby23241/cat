@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        <section id="constructor">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">{{$title}} | {{$subtitle}}</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                       <li><a type="button" class="btn btn-info btn-sm" href="{{site_url()}}hasil/cetak/{{$this->uri->segment(3)}}/{{$this->uri->segment(4)}}">
                            Cetak
                          </a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered " id="data_soal">
                      <thead>
                        @php
                        $no = 1; 
                        @endphp

                        <tr>
                          <th>Nomor</th>
                          <th>Nip</th>
                          <th>Nama Lengkap</th>
                          @foreach(_get_jenis_soal($id_sesi,$id_kategori_ujian) as $row)
                            <th>{{get_jenis_soal($row->id_jenis_soal)}}</th>
                            <th>Passing Grade {{get_jenis_soal($row->id_jenis_soal)}}</th>
                          @endforeach
                          <th>Total Skor</th>
                          <th>Status Kelulusan</th>
                     <!--      <th>Edit</th>
                          <th>Delete</th> -->
                        </tr>
                        
                      </thead>
                      <tbody>
                        @foreach(get_pegawai_sesi($id_sesi,$id_kategori_ujian) as $row)

                        
                              <tr>
                                <td>{{$no++}}</td>
                                <td>{{$row->nip}}</td>
                                <td>{{get_detail_pegawai($row->nip)->nama_lengkap}}</td>
                                @php
                                  $total = 0;
                                  $temp_total = array();
                                  $temp_pass = array();
                                @endphp
                                @foreach(_get_jenis_soal($id_sesi,$id_kategori_ujian) as $rows)
                                    <td>{{_get_details_bobot($row->nip,$id_sesi,$rows->id_jenis_soal)}}</td>
                                    @php
                                    $temp_total[] = _get_details_bobot($row->nip,$id_sesi,$rows->id_jenis_soal);
                                    $total+=_get_details_bobot($row->nip,$id_sesi,$rows->id_jenis_soal)
                                    @endphp
                                    
                                    <td>{{_get_passgrade($id_kategori_ujian,$rows->id_jenis_soal)->passing_grade}}</td>
                                    @php
                                    $temp_pass[] = _get_passgrade($id_kategori_ujian,$rows->id_jenis_soal)->passing_grade;
                                    @endphp
                                @endforeach
                                <td>{{$total}}</td>
                                <td>
                                  @php

                                  $status = array();
                                    for($i=0;$i<count($temp_total);$i++){
                                      if($temp_total[$i]>=$temp_pass[$i])
                                      {
                                        $status[$i] = "Lulus";
                                      }else{
                                        $status[$i] = "Gagal";
                                      }
                                    }

                                    if (in_array('Gagal', $status))
                                    {
                                      echo "<span class='btn btn-danger btn-sm'>Tidak Lulus Passing Grade</span>";
                                    }else{
                                      echo "<span class='btn btn-success btn-sm'>Lulus Passing Grade</span>";
                                    }
                                  @endphp


                                </td>
                                <!-- <td><a href="{{site_url()}}pegawai/view_edit/{{$row->nip}}" class="btn btn-warning btn-sm">Edit</a></td>
                              <td><a href="javascript:void(0)" class="btn btn-danger btn-sm delete" data-id="{{$row->nip}}">Delete</a></td> -->
                              </tr>
                         
                        @endforeach
                      </tbody>
                    </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </div>
        </div>
    </div>

@endsection



