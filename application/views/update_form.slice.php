@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        	 <section id="basic-form-layouts">
	          <div class="row match-height">
	            <div class="col-md-12">
	              <div class="card">
	                <div class="card-header"><br>
                      @if($message!=null)
                         <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong>Perhatian !</strong> {{$message}} <span class="alert-link">Perbaiki</span>dan <span class="alert-link">Simpan</span>
                        </div>
                      @endif
                     

	                  <h4 class="card-title" id="basic-layout-form">Ganti Password</h4>
	                </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <form class="form" method="post" action="{{site_url()}}admin/update">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i>Ganti Password</h4>
                            
                            <div class="form-group">
                                <label>Username</label>
                                 <input type="text" name="username" class="form-control" value="{{$data->username}}">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                 <input type="text" name="password" class="form-control">
                            </div>
                            <input type="submit" name="update" value="Update" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
        </div>
    </div>
</div>

@endsection

