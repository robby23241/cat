@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        	 <section id="basic-form-layouts">
	          <div class="row match-height">
	            <div class="col-md-12">
	              <div class="card">
	                <div class="card-header"><br>
                      @if($message!=null)
                         <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong>Perhatian !</strong> {{$message}} <span class="alert-link">Perbaiki</span>                          dan <span class="alert-link">Simpan</span>
                        </div>
                      @endif
                     

	                  <h4 class="card-title" id="basic-layout-form">Edit Soal Ujian</h4>
	                </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <form class="form" method="post" action="{{site_url()}}soal/ubah">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Soal </h4>
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Jenis Soal</label>
                                             <select id="s" name="bidang_soal" class="form-control" required="">
                                               @foreach(get_bidang()->result() as $row)
                                                    @if($data->id_jenis_soal === $row->id)
                                                      <option value="{{$row->id}}" selected="">{{$row->jenis_soal}}</option>
                                                    @elseif
                                                      <option value="{{$row->id}}">{{$row->jenis_soal}}</option>
                                                    @endif
                                               @endforeach
                                           </select>
                                          
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Tipe Soal</label>
                                         <select id="mySelect" name="check_set" class="form-control" required="" onchange="myFunction()">
                                            @if($data->status=='single')
                                                <option value="single" selected="">Satu kunci jawaban</option>
                                                <option value="multiple">Semua benar</option>
                                            @else
                                                <option value="single">Satu kunci jawaban</option>
                                                <option value="multiple" selected="">Semua benar</option>
                                            @endif

                                          </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Soal</label>
                                        <textarea name="soal" class="form-control" id="soal" required="">{{htmlspecialchars( $data->soal)}}</textarea>
                                    </div>
                                </div>
                            </div>
                           
                            <h4 class="form-section"><i class="fa fa-paperclip"></i> Jawaban </h4>
                            <div class="form-group">
                                <label for="companyName">Jawaban A</label>
                                <textarea name="jawaban_a" class="form-control" id="jawaban_a" required="">{{$data->jawaban_a}}</textarea>
                                @if($data->status=='single')
                                    <div class="form-group" style="display: none;" id="set">
                                @else
                                    <div class="form-group" id="set">
                                @endif
                                    <label>Bobot Nilai A</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_a" value="{{$data->bobot_a}}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban B</label>
                                <textarea name="jawaban_b" class="form-control" id="jawaban_b" required="">{{$data->jawaban_b}}</textarea>
                                @if($data->status=='single')
                                    <div class="form-group" style="display: none;" id="set">
                                @else
                                    <div class="form-group" id="set">
                                @endif
                                    <label>Bobot Nilai B</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_b" value="{{$data->bobot_b}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban C</label>
                                <textarea name="jawaban_c" class="form-control" id="jawaban_c" required="">{{$data->jawaban_c}}</textarea>
                                @if($data->status=='single')
                                    <div class="form-group" style="display: none;" id="set">
                                @else
                                    <div class="form-group" id="set">
                                @endif
                                    <label>Bobot Nilai C</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_c" value="{{$data->bobot_c}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban D</label>
                                <textarea name="jawaban_d" class="form-control" id="jawaban_d" required="">{{$data->jawaban_d}}</textarea>
                                @if($data->status=='single')
                                    <div class="form-group" style="display: none;" id="set">
                                @else
                                    <div class="form-group" id="set">
                                @endif
                                    <label>Bobot Nilai D</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_d" value="{{$data->bobot_d}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban E</label>
                                <textarea name="jawaban_e" class="form-control" id="jawaban_e" required="">{{$data->jawaban_e}}</textarea>
                                @if($data->status=='single')
                                    <div class="form-group" style="display: none;" id="set">
                                @else
                                    <div class="form-group" id="set">
                                @endif
                                    <label>Bobot Nilai E</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_e" value="{{$data->bobot_e}}">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>Kunci Jawaban</label>
                                 <select id="projectinput5" name="kunci_jawaban" class="form-control">
                                     <option value="" @if($data->kunci_jawaban=="") selected="" @endif></option>
	                                <option value="A" @if($data->kunci_jawaban=="A") selected="" @endif>A</option>
	                                <option value="B"  @if($data->kunci_jawaban=="B") selected="" @endif>B</option>
	                                <option value="C"  @if($data->kunci_jawaban=="C") selected="" @endif>C</option>
	                                <option value="D"  @if($data->kunci_jawaban=="D") selected="" @endif>D</option>
	                                <option value="E"  @if($data->kunci_jawaban=="E") selected="" @endif>E</option>
	                              </select>
                            </div>
                            <div class="form-group">
                                <label>Bobot Nilai</label>
                                <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot" required="" value="{{$data->bobot}}">
                            </div>
                        </div>
                        <div class="form-actions">
                            <a href="{{site_url()}}soal" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Cancel
                            </a> <!-- Not working -->
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-check-square-o"></i> Edit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
        </div>
    </div>
</div>

<script src='{{APP_ASSETS}}admin/js/lib/ckeditor12/ckeditor.js'></script>
<script type="text/javascript">
	  CKEDITOR.config.extraAllowedContent = true;
   CKEDITOR.config.pasteFilter = 'p; a[!href]';
     CKEDITOR.replace("soal",{
            filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]; img{*}[*](*)',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
        });

      CKEDITOR.replace("jawaban_a",{
            filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
        });

       CKEDITOR.replace("jawaban_b",{
            filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
        });

        CKEDITOR.replace("jawaban_c",{
            filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
        });

         CKEDITOR.replace("jawaban_d",{
            filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
        });

          CKEDITOR.replace("jawaban_e",{
            filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
        });
</script>
@endsection

