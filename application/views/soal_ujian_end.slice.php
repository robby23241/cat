@extends('user_base') 
@section('content')

<div class="app-content content d-flex justify-content-center align-items-center">
    <div class="content-wrapper">
        <div class="col-xs-12 col-lg-12 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0 rounded">
                <div class="card-header border-0">
                    <div class="card-title text-center">
                        <h2 class="text-center">{{$this->session->userdata("kategori_ujian")}}</h2>
                        <!--  <h4 class="text-center">Submisi Jawaban Ujian</h4>  -->      
                        <h4 class="card-body text-center">
                        <!--  Nilai anda adalah : <b>{{$total_nilai}} </b> -->
                        Terimakasih telah mengikuti ujian ini
                        </h4>    
                        @php

                            $sum = [];
                            foreach ($data as $key => $value) {
                                if (!isset($sum[$value["id_jenis_soal"]]))
                                    {
                                        $sum[$value["id_jenis_soal"]] = $value["bobot"];
                                    }else{
                                        $sum[$value["id_jenis_soal"]] += $value["bobot"];
                                    }
                            }
                           

                        @endphp
                        <table border="1"><tr><td>Jenis Ujian</td><td>Hasil</td></tr>
                            @foreach($sum as $row=>$key)
                                <tr><td>{{get_jenis_soal($row)}}</td><td>{{$key}}</td></tr>
                            @endforeach
                        </table>
                        <a class="nav-link" href="{{site_url()}}tes/logout_tes"><span class="btn btn-main">Keluar</span></a>
                    </div>                       
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection