@extends('main_base')


@section('content')
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- Stats -->
        <div class="row">
          
          <div class="col-xl-3 col-lg-6 col-12">
            <div class="card">
              <div class="card-content">
                <div class="media align-items-stretch">
                  <div class="p-2 text-center bg-primary bg-darken-2">
                    <i class="icon-user font-large-2 white"></i>
                  </div>
                  <div class="p-2 bg-gradient-x-primary white media-body">
                    <h5>Total Pegawai</h5>
                    <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i>{{$datapeg}}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @foreach($data as $row)
          <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                      <div class="card-content">
                          <div class="card-body">
                              <div class="media">
                                  <div class="media-body text-left w-100">
                                      <h3 class="primary">{{$row->jumlah}}</h3>
                                      <span>Soal {{$row->jenis_soal}}</span>
                                  </div>
                                  <div class="media-right media-middle">
                                      <i class="icon-social-dropbox primary font-large-2 float-right"></i>
                                  </div>
                              </div>
                              <div class="progress progress-sm mt-1 mb-0">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
          @endforeach
        
        </div>
        <!--/ Stats -->
        <!--/ Basic Horizontal Timeline -->
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  @endsection