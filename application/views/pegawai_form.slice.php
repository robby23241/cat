@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
             <section id="basic-form-layouts">
              <div class="row match-height">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header"><br>
                      @if($message!=null)
                         <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong>Perhatian !</strong> {{$message}} <span class="alert-link">Perbaiki</span>dan <span class="alert-link">Simpan</span>
                        </div>
                      @endif
                     
                      <h4 class="card-title" id="basic-layout-form">Tambah Data Pegawai</h4>
                      <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <form class="form" method="post" action="{{site_url()}}pegawai/insert">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Data Pegawai </h4>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Nip</label>
                                        <input type="text" name="nip" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Nama Lengkap</label>
                                        <input type="text" name="nama_lengkap" class="form-control">
                                    </div>
                                </div>
                            </div>

                              <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Kategori Ujian</label>
                                        <select name="id_kategori_ujian" class="form-control" id="Kategori">
                                          @foreach(get_kategori()->result() as $rows)
                                            <option value="{{$rows->id}}">{{$rows->kategori}}</option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>


                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Sesi Ujian</label>
                                        <select name="id_sesi" id="id_sesi" class="form-control">
                                          <option>No Selected</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <button type="button" class="btn btn-warning">
                                <i class="ft-x"></i> Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-check-square-o"></i> Save
                            </button>
                            <button type="button" class="btn btn-success" data-toggle="modal"
                          data-target="#bootstrap">
                            Import CSV
                          </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h3 class="modal-title" id="myModalLabel35"> Impor Berkas </h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" enctype="multipart/form-data" action="{{site_url()}}pegawai/import_csv">
        <div class="modal-body">
          <fieldset class="form-group floating-label-form-group">
            <input type="file" name="csv_file" id="csv_file"  />
          </fieldset>
        </div>
        <div class="modal-footer">
          <input type="reset" class="btn btn-quest-list" data-dismiss="modal" value="Close">
          <input type="submit" class="btn btn-main" value="Simpan" name="Upload">
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section("javascript")
<script type="text/javascript">
   $(document).ready(function() {
            $('#Kategori').click(function(){ 
                var id=$(this).val();
                $.ajax({
                    url:"{{site_url()}}pegawai/get_sesi/"+id,
                    method: "POST",
                    data: {id: id},
                    dataType: "json",
                    success: function(data){

                        var html = '';
                        var i;
                        if(data.length>0){
                          for(i=0; i<data.length; i++){
                              html += '<option value='+data[i].id_sesi+'>'+data[i].nama_sesi+'</option>';
                          }
                        }else{
                          html = '<option>No Selected</option>';
                        }
                  
                        $('#id_sesi').html(html);
 
                    }
                });
                return false;
            }); 
         });     
  
</script>
@endsection

