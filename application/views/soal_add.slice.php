@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        	 <section id="basic-form-layouts">
	          <div class="row match-height">
	            <div class="col-md-12">
	              <div class="card">
	                <div class="card-header"><br>
                      @if($message!=null)
                         <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong>Perhatian !</strong> {{$message}} <span class="alert-link">Perbaiki</span>dan <span class="alert-link">Simpan</span>
                        </div>
                      @endif
                     

	                  <h4 class="card-title" id="basic-layout-form">Tambah Soal Ujian Dinas</h4>
	                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	                  <div class="heading-elements">
	                    <ul class="list-inline mb-0">
                          <li><button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                          data-target="#bootstrap">
                            Import CSV
                          </button></li>
	                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
	                    </ul>
	                  </div>

	                </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <form class="form" method="post" action="{{site_url()}}soal/simpan_soal">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Soal </h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Jenis Soal</label>
                                             <select id="s" name="bidang_soal" class="form-control" required="">
                                               @foreach(get_bidang()->result() as $row)
                                                     <option value="{{$row->id}}">{{$row->jenis_soal}}</option>
                                               @endforeach
                                           </select>
                                          
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Tipe Soal</label>
                                         <select id="mySelect" name="check_set" class="form-control" required="" onchange="myFunction()">
                                            <option value="single">Satu kunci jawaban</option>
                                            <option value="multiple">Semua benar</option>
                                          </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Soal</label>
                                        <textarea name="soal" class="form-control" id="soal" ></textarea>
                                    </div>
                                </div>
                            </div>
                           
                            <h4 class="form-section"><i class="fa fa-paperclip"></i> Jawaban </h4>
                            <div class="form-group">
                                <label for="companyName">Jawaban A</label>
                                <textarea name="jawaban_a" class="form-control" id="jawaban_a" required=""></textarea>
                                <div class="form-group" style="display: none;" id="set">
                                    <label>Bobot Nilai A</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_a" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban B</label>
                                <textarea name="jawaban_b" class="form-control" id="jawaban_b" required=""></textarea>
                                <div class="form-group" style="display: none;" id="set">
                                    <label>Bobot Nilai B</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_b" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban C</label>
                                <textarea name="jawaban_c" class="form-control" id="jawaban_c" required=""></textarea>
                                <div class="form-group" style="display: none;" id="set">
                                    <label>Bobot Nilai C</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_c" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban D</label>
                                <textarea name="jawaban_d" class="form-control" id="jawaban_d" required=""></textarea>
                                <div class="form-group" style="display: none;" id="set">
                                    <label>Bobot Nilai D</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_d">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban E</label>
                                <textarea name="jawaban_e" class="form-control" id="jawaban_e" required=""></textarea>
                                <div class="form-group" style="display: none;" id="set">
                                    <label>Bobot Nilai E</label>
                                    <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot_e">
                                </div>
                            </div>
                            
                            <div class="form-group" id="unset">
                                <label>Kunci Jawaban</label>
                                 <select id="projectinput5" name="kunci_jawaban" class="form-control" >
	                                <option value="A" selected="">A</option>
	                                <option value="B">B</option>
	                                <option value="C">C</option>
	                                <option value="D">D</option>
	                                <option value="E">E</option>
	                              </select>
                            </div>
                            <div class="form-group" id="unset">
                                <label>Bobot Nilai</label>
                                <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot">
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Cancel
                            </button> <!-- Not Working -->
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-check-square-o"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
                          aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h3 class="modal-title" id="myModalLabel35"> Impor Berkas </h3>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form method="post" enctype="multipart/form-data" action="{{site_url()}}soal/import_csv">
                                  <div class="modal-body">
                                    <fieldset class="form-group floating-label-form-group">
                                     <input type="file" name="csv_file" id="csv_file" required accept=".csv" />
     
                                    </fieldset>
                                  </div>
                                  <div class="modal-footer">
                                    <input type="reset" class="btn btn-outline-secondary btn-sm" data-dismiss="modal"
                                    value="close">
                                    <input type="submit" class="btn btn-outline-primary btn-sm" value="Simpan" name="upload">
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

<script src='{{APP_ASSETS}}admin/js/lib/ckeditor12/ckeditor.js'></script>
<script type="text/javascript">

   CKEDITOR.config.extraAllowedContent = true;
   CKEDITOR.config.pasteFilter = 'p; a[!href]';
	 CKEDITOR.replace("soal",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]; img{*}[*](*)',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  editor.setData( '' );
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
	    });

	  CKEDITOR.replace("jawaban_a",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]; img{*}[*](*)',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  editor.setData( '' );
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
	    });

	   CKEDITOR.replace("jawaban_b",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]; img{*}[*](*)',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  editor.setData( '' );
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
	    });

	    CKEDITOR.replace("jawaban_c",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]; img{*}[*](*)',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  editor.setData( '' );
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
	    });

	     CKEDITOR.replace("jawaban_d",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]; img{*}[*](*)',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  editor.setData( '' );
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
	    });

	      CKEDITOR.replace("jawaban_e",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder',
          allowedContent: 'p b i; a[!href]; img{*}[*](*)',
          on: {
              instanceReady: function( evt ) {
                  var editor = evt.editor;

                  editor.filter.check( 'h1' ); // -> false
                  editor.setData( '' );
                  // Editor contents will be:
                  '<p><i>Foo</i></p><p>Bar <a href="http://foo.bar">foo</a></p>'
              }
          }
	    });
</script>
<script>
function myFunction() {
  var x = document.getElementById("mySelect").value;
  var getAll = document.querySelectorAll("[id='set']");
  var unSet = document.querySelectorAll("[id='unset']");
  if(x=='multiple')
  {
   
    for(var i = 0; i < getAll.length; i++) 
        getAll[i].style.display='inline';
    for(var i = 0; i < unSet.length; i++) 
        unSet[i].style.display='none';
    
  }else if(x=='single'){
    for(var i = 0; i < getAll.length; i++) 
        getAll[i].style.display='none';
    for(var i = 0; i < unSet.length; i++) 
        unSet[i].style.display='inline';
  }
}
</script>
@endsection

