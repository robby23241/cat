@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        <section id="constructor">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">{{$title}}</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a href="{{site_url()}}soal/page_add"><i class="ft-plus"></i></a></li>
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="row">
                  <div class="col-4 ml-3">
                    <form id="myForm" method="post" action="{{site_url()}}soal/search_by">
                      <select name="kode_soal" class="form-control" id="ajaxSubmit" onchange="this.form.submit()">
                        <option value="">---Pilih---</option>
                        @foreach(get_bidang()->result() as $rows)
                        <option value="{{$rows->id}}">{{$rows->jenis_soal}}</option>
                        @endforeach
                      </select>
                    </form>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered" id="data_soal">
                      <thead>
                      	@php $no = 1; @endphp

                        <tr  align=center ng-repeat="user in users | fiter:search">
                          <th>Nomor</th>
                          <th>Soal</th>
                          <th>Jenis Soal</th>
                      <!--     <th>Jawaban A</th>
                          <th>Jawaban B</th>
                          <th>Jawaban C</th>
                          <th>Jawaban D</th>
                          <th>Jawaban E</th>
                          <th>Kunci Jawaban</th> -->
                          <th>Tipe Soal</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                        
                      </thead>
                      <tbody>
                      	@foreach($data as $row)
	                        <tr>
	                          <td align=center>{{$no++}}</td>
                            <td align=center>{{$row->soal}}</td>
                            <td align=center>{{$row->jenis_soal}}</td>
	                          <!-- <td align=center>{{$row->jawaban_a}}</td>
	                          <td align=center>{{$row->jawaban_b}}</td>
	                          <td align=center>{{$row->jawaban_c}}</td>
	                          <td align=center>{{$row->jawaban_d}}</td>
	                          <td align=center>{{$row->jawaban_e}}</td>
	                          <td align=center>{{$row->kunci_jawaban}}</td> -->
                            @if($row->status=='single')
                            <td align=center><span class="badge badge badge-pill badge-info">{{$row->status}}</span></td>
                            @else
                            <td align=center><span class="badge badge badge-pill badge-warning">{{$row->status}}</span></td>
                            @endif
                            <td><a href="{{site_url()}}soal/ubah_soal/{{$row->id}}" class="btn btn-success btn-sm edit">Edit</a></td>
                            <td><a href="javascript:void(0)" class="btn btn-danger btn-sm delete" data-id="{{$row->id}}">Delete</a></td>
	                        </tr>
                        @endforeach
                      </tbody>
                    </table>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </div>
		        </section>
		    </div>
		</div>
	</div>

@endsection

@section("javascript")

<script type="text/javascript">
  
    $('#data_soal').dataTable({
            "searching": true,
            "ordering": true,
            "paging": "false"
        });

</script> 
<script type="text/javascript">


  $(document).ready(function() {


    $('#data_soal tbody').on('click', 'a.delete', function () {
        var id = $(this).data().id;
        swal({
            title: "Are you sure?",
            text: "Delete the file?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
          },
           function(isConfirm){
             if (isConfirm) {
              $.ajax({
                  url:"{{site_url()}}soal/hapus_soal/"+id,
                  type: "POST",
                  data: {id: id},
                  dataType: "html",
                  success: function (res) {
                    if(res.status==true){
                      swal("Done!","Delete success","success");
                      
                    }
                      location.reload();
                  }
              });
            }else{
                      swal("Cancelled", "Delete cancelled", "error");
            } 
         })

      });

      

    });
</script>
  @endsection

