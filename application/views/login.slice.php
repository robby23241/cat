<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>Login</title>
  <link rel="apple-touch-icon" href="{{APP_ASSETS}}images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="{{APP_ASSETS}}images/ico/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/vendors.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/forms/icheck/icheck.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/forms/icheck/custom.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN STACK CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/app.css">
  <!-- END STACK CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/core/colors/palette-gradient.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/pages/login-register.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}assets-plus/css/style.css">
  <!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-menu 1-column  bg-full-screen-image menu-expanded blank-page blank-page"
data-open="click" data-menu="vertical-menu" data-col="1-column">
  <!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body bg-img" style="background-image: url('{{APP_ASSETS}}images/backgrounds/bg_img1.jpg');">
      <section class="flexbox-container">
        <div class="col-12 d-flex align-items-center justify-content-center">
          <div class="col-xs-12 col-lg-4 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0 rounded">
              <div class="card-header border-0">
                <div class="card-title text-center">
                  <img src="{{APP_ASSETS}}images/logo/sijantan_logo.png" alt="Logo Si Jantan" width="35%" height="35%">
                </div>
                <h6 class="card-subtitle text-muted text-center pt-2">
                  <span><h4>Login</h4></span>
                </h6>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <form class="form-horizontal" method="post" action="{{site_url()}}login/prosesLogin" novalidate>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" autocomplete="off" class="form-control" id="user-name" name="username" placeholder="Username" required >
                      <div class="form-control-position">
                        <i class="ft-user"></i>
                      </div>
                    </fieldset>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="password" autocomplete="off" class="form-control" id="user-password" name="password" placeholder="Password" required >
                      <div class="form-control-position">
                        <i class="fa fa-key"></i>
                      </div>
                    </fieldset>
                    <button type="submit" class="btn btn-main btn-block"><i class="ft-unlock"></i> Login</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <!-- BEGIN VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/forms/validation/jqBootstrapValidation.js"
  type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="{{APP_ASSETS}}js/core/app-menu.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/core/app.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/scripts/customizer.js" type="text/javascript"></script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{APP_ASSETS}}js/scripts/forms/form-login-register.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>