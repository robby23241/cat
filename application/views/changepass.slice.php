@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form">Ganti Password</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" method="post" action="">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="passlama">Password Lama</label>
                                                    <input type="password" name="passlama" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="passbaru">Password Baru</label>
                                                    <input type="password" name="passbaru" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="repassbaru">Ketik Ulang Password Baru</label>
                                                    <input type="password" name="repassbaru" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i> Cancel</button>
                                            <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-check-square-o"></i> Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

