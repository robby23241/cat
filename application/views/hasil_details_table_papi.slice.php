@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        <section id="constructor">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">{{$title}} | {{$subtitle}}</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                       <li><a type="button" class="btn btn-info btn-sm" href="{{site_url()}}hasil/cetak/{{$this->uri->segment(3)}}/{{$this->uri->segment(4)}}">
                            Cetak
                          </a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered " id="data_soal">
                      <thead>
                        @php
                        $no = 1; 
                        @endphp

                        <tr>
                          <th>Nomor</th>
                          <th>Nip</th>
                          <th>Nama Lengkap</th>
                          @foreach($thead as $row)
                            <th>{{$row}}</th>
                          @endforeach
                        </tr>
                        
                      </thead>
                      <tbody>
                        @foreach(get_pegawai_sesi($id_sesi,$id_kategori_ujian) as $row)

                        
                              <tr>
                                <td>{{$no++}}</td>
                                <td>{{$row->nip}}</td>
                                <td>{{get_detail_pegawai($row->nip)->nama_lengkap}}</td>
                                @foreach($thead as $row_val)
                                  <td>{{get_jawaban_papi($row->nip,$row_val)}}</td>
                                @endforeach
                              </tr>
                         
                        @endforeach
                      </tbody>
                    </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </div>
        </div>
    </div>

@endsection



