@php
defined('BASEPATH') OR exit('No direct script access allowed');
@endphp

<!doctype html>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="CAT Prototype">
  <title>Admin Panel</title>  <!-- Change per each menu clicked? -->
  <link rel="apple-touch-icon" href="{{APP_ASSETS}}images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="{{APP_ASSETS}}images/ico/favicon.ico">
  <!-- Font Style start-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- Font style end -->
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/vendors.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/extensions/unslider.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/weather-icons/climacons.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}fonts/meteocons/style.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN Theme CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/app.css">
  <!-- END Theme CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}fonts/simple-line-icons/style.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}/css/core/colors/palette-gradient.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/pages/timeline.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}assets-plus//css/style.css">
  END Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/datatable/datatables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/responsive.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/colReorder.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/buttons.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/datatable/buttons.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/tables/extensions/fixedHeader.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/sweetalert/sweetalert.css"/>

</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="2-columns">
  <!-- fixed-top-->
  <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item">
            <a class="navbar-brand" href="{{site_url()}}welcome">
              <img class="img-xs brand-logo" alt="logo" src="{{APP_ASSETS}}images/logo/bintankab_logo.png"> <!-- image size fix-->
              <h2 class="brand-text">BKPSDM</h2>
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
          </ul>
          <ul class="nav navbar-nav float-right">
            <li class="dropdown dropdown-user nav-item">
              <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="avatar avatar-online">
                  <img src="{{APP_ASSETS}}images/logo/diskominfo_logo.png" alt="avatar"><i></i></span>
                <span class="user-name">Admin</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{site_url()}}login/logout"><i class="ft-power"></i> Logout</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

        <li class="navigation nav-item"><a href="{{site_url()}}index"><i class="ft-home"></i><span class="menu-title" data-i18n="">Admin</span></a>
          <ul class="menu-content">
           <li><a class="menu-item" href="{{site_url()}}pegawai">Data Pegawai</a></li>
          </ul>
        </li>
        <li><a class="menu-item" href="#"><i class="ft-monitor"></i>Soal Ujian</a>
          <ul class="menu-content">
            <li><a class="menu-item" href="{{site_url()}}jenis_soal">Jenis Soal</a></li>
            <li><a class="menu-item" href="{{site_url()}}soal">Soal</a></li>
            <li><a class="menu-item" href="{{site_url()}}soal/papi_add">Soal PAPI</a></li>
            <li><a class="menu-item" href="{{site_url()}}hasil">Hasil Ujian</a></li>
          </ul>

     <!--    <li class="navigation nav-item"><a href="#"><i class="ft-home"></i><span class="menu-title" data-i18n="">Admin</span><span class="badge badge badge-primary badge-pill float-right mr-2">3</span></a>
            <ul class="menu-content">
              <li><a class="menu-item" href="{{site_url()}}pegawai">Data Pegawai</a></li>
            </ul>

        </li>
        <li class="navigation nav-item"><a href="#"><i class="ft-monitor"></i><span class="menu-title">Soal Ujian</span></a>
            <ul class="menu-content">
              <li><a class="menu-item" href="{{site_url()}}jenis_soal">Jenis Soal</a></li>
              <li><a class="menu-item" href="{{site_url()}}soal">Soal</a></li>
              <li><a class="menu-item" href="{{site_url()}}hasil">Hasil Ujian</a></li>
            </ul>
          </li> -->
        <li class="navigation nav-item"><a href="#"><i class="ft-monitor"></i><span class="menu-title">Manajemen Ujian</span></a>
          <ul class="menu-content">
            <li><a class="menu-item" href="{{site_url()}}kategori">Kategori Ujian</a></li>
            <li><a class="menu-item" href="{{site_url()}}sesi">Sesi Ujian</a></li>
            <li><a class="menu-item" href="{{site_url()}}manajemen_ujian">Manajemen Soal Ujian</a></li>
          </ul>
        </li>


        <li><a class="menu-item" href="#"><i class="ft-settings"></i>Setting</a>


          <ul class="menu-content">
            <li><a class="menu-item" href="{{site_url()}}admin/update_form">Ganti Password</a></li>
            <li><a class="menu-item" href="{{site_url()}}login/logout">Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  @yield('content')

    <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <b>DISKOMINFO BINTAN</b>, All rights reserved.</span>
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/charts/raphael-min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="{{APP_ASSETS}}js/core/app-menu.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/core/app.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/scripts/customizer.js" type="text/javascript"></script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{APP_ASSETS}}js/scripts/pages/dashboard-ecommerce.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
  <script src="{{APP_ASSETS}}js/scripts/tables/datatables-extensions/datatable-responsive.js"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.colReorder.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/dataTables.fixedHeader.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="{{APP_ASSETS}}vendors/sweetalert/sweetalert.min.js"></script>
</body>
</html>
  @section('javascript')

  @show
</body>
</html>