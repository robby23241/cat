@extends('user_base') 
<style>
    #timer_place{
        margin:0 auto;
        text-align:center;
    }
    #timer{
        border-radius:7px;
        border:2px solid gray;
        padding:7px;
        font-size:2em;
        font-weight:bolder;
    }
</style>

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="col-12">
                <h3 class="text-center">SOAL {{$this->session->userdata("kategori_ujian")}}</h3>
            </div>
        </div>
        <!-- Section Pertanyaan -->
        <div class="content-body">
            <div id="pertanyaan">
                <div class="row bg-white">
                    <div class="col-lg-9 col-xs-12 padding">
                        <div class="col-12 padding ">
                            <div class="col col-12 padding">
                                <div class="col-12">
                                    <!-- Pertanyaan Ujian per soal-->
                                    @foreach($first as $row)
                                    <h4><span>Soal : <b>TIU</b></span></h4>
                                    
                                    <p class="card-body">
                                        {{$row->id}} . {{str_ireplace('<p>','',$row->soal)}}
                                    </p>
                                    <!-- jawaban soal-->
                                    <form method="post" action="{{site_url()}}Ujian_tiu/{{$fungsi}}">
                                        <input type="hidden" name="id_soal" value="{{$row->id}}">
                                        <input type="hidden" name="id" value="{{$id}}">

                                    <div class="col-12">
                                        <div class="col-12">
                                            <label class="answer-box">
                                                @if($checked=='A')
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer1" value="A" checked="">
                                                @else
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer1" value="A">
                                                @endif
                                                <div class="card card-body padding d-flex flex-row justify align-items-center">
                                                    <div class="card-text"><span>A. </span> {{$row->jawaban_a}}
                                                    </div>
                                                </div>
                                                
                                            </label>
                                            
                                            <label class="answer-box">
                                                @if($checked=='B')
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer2" value="B" checked="">
                                                @else
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer2" value="B">
                                                @endif
                                                <div class="card card-body padding d-flex flex-row justify align-items-center">
                                                    <div class="card-text"><span>B. </span> {{$row->jawaban_b}}
                                                    </div>
                                                </div>
                                            </label>
                                            
                                            <label class="answer-box">
                                                @if($checked=='C')
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer3" value="C" checked="">
                                                @else
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer3" value="C">
                                                    @endif
                                                <div class="card card-body padding d-flex flex-row justify align-items-center">
                                                    <div class="card-text"><span>C. </span> {{$row->jawaban_c}}
                                                    </div>
                                                </div>
                                            </label>
                                            
                                            <label class="answer-box">
                                                @if($checked=='D')
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer4" value="D" checked="">
                                                @else
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer4" value="D">
                                                @endif
                                                <div class="card card-body padding d-flex flex-row justify align-items-center">
                                                    <div class="card-text"><span>D. </span> {{$row->jawaban_d}}
                                                    </div>
                                                </div>
                                            </label>
                                            
                                            <label class="answer-box">
                                                @if($checked=='E')
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer5" value="E" checked="">
                                                @else
                                                <input type="radio" name="answer" class="card-input-element d-none" id="answer5" value="E">
                                                    @endif
                                                <div class="card card-body padding d-flex flex-row justify align-items-center">
                                                    <div class="card-text"><span>E. </span> {{$row->jawaban_e}}
                                                    </div>
                                                </div>
                                            </label>
                                            
                                        </div>
                                        <!-- jawaban soal end-->
                                        <!-- Prev-next Button -->
                                        <div class="col-12">
                                            <!--   <a href="#" class="btn btn-main float-left">Prev</a> -->
                                            <input type="submit" class="btn btn-main float-right" value="Next">
                                        </div>
                                        <!-- Prev-next Button -->
                                    </div>
                                </form>

                                @endforeach
                            </div>
                            <!-- Section Soal End -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 padding">
                <p class="text-right">Sisa Waktu : <span id="timer">00 : 00 : 00</span> </p>
                    <div id="list-pertanyaan">
                        <div class="col-12">
                            <a class="btn btn-quest-list margin-question" tabindex="2" data-toggle="modal" data-target="#modalsubmit">Submit Jawaban</a> <!--Button Trigger-->
                        </div>
                        <div class="col-12">
                            <h4><b>Nomor Soal</b></h4>
                            <!-- <a href="#" class="btn btn-quest-list margin-question active">1</a>
                            <a href="#" class="btn btn-quest-list margin-question">2</a>
                            <a href="#" class="btn btn-quest-list margin-question">3</a>
                            <a href="#" class="btn btn-quest-list margin-question">4</a>
                            <a href="#" class="btn btn-quest-list margin-question">5</a>
                            <a href="#" class="btn btn-quest-list margin-question active">6</a>
                            <a href="#" class="btn btn-quest-list margin-question">7</a>
                            <a href="#" class="btn btn-quest-list margin-question">8</a>
                            <a href="#" class="btn btn-quest-list margin-question active">9</a>
                            <a href="#" class="btn btn-quest-list margin-question">10</a> -->
                             @foreach($all as $row)
                                  
                                    <a class="btn btn-quest-list margin-question inactive" href="{{site_url()}}Ujian_tiu/ujian_tiu/{{$row->id}}">{{$row->id}}</a>
                                  
                            @endforeach
                        </div>
                        <br>
                          <!-- Modal -->
                            <div class="modal fade" id="modalsubmit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="title">Mengakhiri Ujian</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <span><h3>Apakah anda ingin submit jawaban?</h3></span><br>
                                            Catatan:<br>
                                            Setelah submit jawaban, anda tidak bisa mengubah atau mengisi jawaban kembali
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
                                            <a href="{{site_url()}}soal_ujian/end_ujian" class="btn btn-main margin-question" id="button_save">Submit Jawaban</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- Modal End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section("javascript")

<script type="text/javascript">
    function waktuHabis(){
        window.location.href = "{{site_url()}}soal_ujian/end_ujian";
    }
    function hampirHabis(periods){
        if($.countdown.periodsToSeconds(periods) == 60){
            $(this).css({color:"red"});
        }
    }
    $(function(){
        var waktu = {{get_time_tiu()}}; 
        var sisa_waktu = waktu - {{$lewat}};
        var longWayOff = sisa_waktu;
        $("#timer").countdown({
            until: longWayOff,
            compact:true,
            onExpiry:waktuHabis,
            onTick: hampirHabis
        });
    })
</script>
@endsection