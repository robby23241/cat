@extends('user_base') 
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="col-12">   
                <h1 class="text-center">Halaman Notifikasi</h1>
            </div>
        </div>
        <div class="content-body">
            <div class="row bg-white">
                <div class="col-12 padding ">    
                    <p class="card-body text-center">
                         <b>{{$message}} </b>
                         <a class="nav-link" href="{{site_url()}}tes/logout_tes"><span class="btn btn-main">Keluar</span></a>
                    </p>    
                </div>                     
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection