@extends('user_base') 
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
       <!--  <div class="content-header row">
            <div class="col-12 pb-2">   
                <h1 class="text-center">SOAL {{_get_data_sesi($this->session->userdata('id_sesi'))->kategori}}</h1>
            </div>
        </div> -->
        <div class="content-body">
            <div class="row bg-white">
                <div class="col-12 padding pt-2">
                    <form class="card col-xs-12 col-md-10 col-lg-6 justify-content-center offset-lg-3 offset-md-1 p-2">
                    <h4 class="text-center">Data Peserta {{_get_data_sesi($this->session->userdata('id_sesi'))->kategori}} <i><u>{{_get_data_sesi($this->session->userdata('id_sesi'))->nama_sesi}}</i></u></h4>
                        <div class="form-group-row">
                            <label for="unip" class="col form-control-plaintext">NIP</label>
                            <div class="col">
                                <input type="text" readonly class="form-control" id="unip" value="{{$this->session->userdata('nip')}}" readonly>           
                            </div>
                        </div>
                        <div class="form-group-row">
                            <label for="uname" class="col form-control-plaintext">Nama Peserta</label>
                            <div class="col">
                                <input type="text" readonly class="form-control" id="uname" value="{{$this->session->userdata('nama')}}" readonly>           
                            </div>
                        </div>
                        <div class="form-group-row">
                            <label for="utglujian" class="col form-control-plaintext">Tanggal Ujian </label>
                            <div class="col">
                                <input type="text" readonly class="form-control" id="utglujian" value="{{_get_data_sesi($this->session->userdata('id_sesi'))->waktu}}" readonly>           
                            </div>
                        </div>
                        <div class="form-group-row">
                            <label for="uujian" class="col form-control-plaintext">Ujian</label>
                            <div class="col">
                                <input type="text" readonly class="form-control" id="uujian" value="{{_get_data_sesi($this->session->userdata('id_sesi'))->kategori}}" readonly>           
                            </div>
                        </div>
                    </form>
                </div>
                    <div class="col-12 padding">
                        <p class="card-body text-center">
                            Setelah Ujian dimulai, timer akan tetap berjalan hingga anda menyelesaikan ujian
                        </p>    
                        
                        <div class="col-12 text-center">
                            <a href="{{site_url()}}soal_ujian/main_ujian" class="col-md-2 btn btn-main">Mulai</a>
                        </div>
                    </div>     
                </div>                          
            </div>
        </div>
    </div>
</div>  
@endsection