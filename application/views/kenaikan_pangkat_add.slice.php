@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        	 <section id="basic-form-layouts">
	          <div class="row match-height">
	            <div class="col-md-12">
	              <div class="card">
	                <div class="card-header"><br>
                      @if($message!=null)
                         <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong>Perhatian !</strong> {{$message}} <span class="alert-link">Perbaiki</span>dan <span class="alert-link">Simpan</span>
                        </div>
                      @endif
                     

	                  <h4 class="card-title" id="basic-layout-form">Tambah Soal Kenaikan Pangkat</h4>
	                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
	                  <div class="heading-elements">
	                    <ul class="list-inline mb-0">
	                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
	                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
	                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
	                    </ul>
	                  </div>
	                </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <form class="form" method="post" action="{{site_url()}}kenaikan_pangkat/simpan_soal">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Soal </h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="projectinput1">Soal</label>
                                        <textarea name="soal" class="form-control" id="soal" required=""></textarea>
                                    </div>
                                </div>
                            </div>
                           
                            <h4 class="form-section"><i class="fa fa-paperclip"></i> Jawaban </h4>
                            <div class="form-group">
                                <label for="companyName">Jawaban A</label>
                                <textarea name="jawaban_a" class="form-control" id="jawaban_a" required=""></textarea>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban B</label>
                                <textarea name="jawaban_b" class="form-control" id="jawaban_b" required=""></textarea>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban C</label>
                                <textarea name="jawaban_c" class="form-control" id="jawaban_c" required=""></textarea>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban D</label>
                                <textarea name="jawaban_d" class="form-control" id="jawaban_d" required=""></textarea>
                            </div>

                            <div class="form-group">
                                <label for="companyName">Jawaban E</label>
                                <textarea name="jawaban_e" class="form-control" id="jawaban_e" required=""></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>Kunci Jawaban</label>
                                 <select id="projectinput5" name="kunci_jawaban" class="form-control" required="">
	                                <option value="A" selected="">A</option>
	                                <option value="B">B</option>
	                                <option value="C">C</option>
	                                <option value="D">D</option>
	                                <option value="E">E</option>
	                              </select>
                            </div>
                            <div class="form-group">
                                <label>Bobot Nilai</label>
                                <input type="text" class="form-control col-md-4" placeholder="Nilai Bobot" name="bobot" required="">
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" class="btn btn-warning mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
        </div>
    </div>
</div>

<script src='{{APP_ASSETS}}admin/js/lib/ckeditor12/ckeditor.js'></script>
<script type="text/javascript">
	 CKEDITOR.replace("soal",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	  CKEDITOR.replace("jawaban_a",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	   CKEDITOR.replace("jawaban_b",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	    CKEDITOR.replace("jawaban_c",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	     CKEDITOR.replace("jawaban_d",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	      CKEDITOR.replace("jawaban_e",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });
</script>
@endsection

