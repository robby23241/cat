@extends("main_base")
@section("content")

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        	 <section id="basic-form-layouts">
	          <div class="row match-height">
	            <div class="col-md-12">
	              <div class="card">
	                <div class="card-header"><br>
                      @if($message!=null)
                         <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong>Perhatian !</strong> {{$message}} <span class="alert-link">Perbaiki</span>dan <span class="alert-link">Simpan</span>
                        </div>
                      @endif
                     

	                  <h4 class="card-title" id="basic-layout-form">Edit Kategori Ujian</h4>
	                </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <form class="form" method="post" action="{{site_url()}}kategori/update">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Form Ujian </h4>
                            <div class="form-group">
                                <label>Kategori Ujian</label>
                                 <input type="text" name="kategori_ujian" class="form-control" value="{{$data->kategori}}">
                            </div>

                            <div class="form-group">
                                <label>Waktu Pengerjaan Ujian</label>
                                 <input type="text" name="waktu" class="form-control" value="{{$data->waktu_pengerjaan}}">
                            </div>

                            <div class="form-group">
                                <label>Jumlah Soal</label>
                                 <input type="number" name="jlh_soal" class="form-control" value="{{$data->jlh_soal}}">
                            </div>
                        </div>
                        <div class="form-actions">
                            <a href="{{site_url()}}kategori" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Cancel
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-check-square-o"></i> Edit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
        </div>
    </div>
</div>

<script src='{{APP_ASSETS}}admin/js/lib/ckeditor12/ckeditor.js'></script>
<script type="text/javascript">
	 CKEDITOR.replace("soal",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	  CKEDITOR.replace("jawaban_a",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	   CKEDITOR.replace("jawaban_b",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	    CKEDITOR.replace("jawaban_c",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	     CKEDITOR.replace("jawaban_d",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });

	      CKEDITOR.replace("jawaban_e",{
	        filebrowserImageBrowseUrl : '{{APP_ASSETS}}admin/js/lib/ckeditor12/kcfinder'
	    });
</script>
@endsection

