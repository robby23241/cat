<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define("APP_ASSETS", base_url('assets/'));
define('APP_NAME', "CAT BKD BINTAN");

date_default_timezone_set("Asia/Bangkok");

function get_kategori()
{
	$ci =& get_instance();
	$query = $ci->db->get("kategori_ujian");

	return $query;
}

function get_kategori_ujian()
{
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('manajemen_ujian');
	$ci->db->join('kategori_ujian', 'kategori_ujian.id = manajemen_ujian.id_kategori_ujian');
	$query = $ci->db->get();

	return $query;
}

function get_bidang()
{
	$ci =& get_instance();
	$query = $ci->db->get("jenis_soal");

	return $query;
}

function get_bidang_papi()
{
	$ci =& get_instance();
	$query = $ci->db->get_where("jenis_soal",array("jenis_soal"=>"PAPI"));

	return $query;
}

function get_bidang_id($id)
{
	$ci =& get_instance();
	$ci->db->where("id",$id);
	$query = $ci->db->get("jenis_soal");

	return $query->row();
}

function get_bidang_name($name)
{
	$ci =& get_instance();
	$ci->db->where("jenis_soal",$name);
	$query = $ci->db->get("jenis_soal");

	return $query->row();
}

function get_jenis_soal($id)
{
	$ci =& get_instance();
	$ci->db->where("id",$id);
	$query = $ci->db->get("jenis_soal");
	$row = $query->row();
	return $row->jenis_soal;
}

function cek_soal_exist($id,$nip,$id_sesi)
{
	$ci =& get_instance();
	$query = $ci->db->query("select * from jawaban a join pegawai b on a.nip=b.nip where a.id_soal='$id' and a.nip='$nip' and a.id_sesi='$id_sesi' ");
	$row = $query->row();
	if($query->num_rows()>0){
		if($row->jawaban!=''){
			return true;
			
		}if ($row->jawaban=='') {
				return false;
		}else{
			return false;
		}
	}
}

function get_time ($nip)
{
	$ci =& get_instance();
	$query = $ci->db->query("select a.waktu_pengerjaan from kategori_ujian a join sesi b on a.id=b.id_kategori_ujian join pegawai c on c.id_sesi = b.id_sesi where c.nip='$nip' ");
	$row = $query->row();
	$waktu = $row->waktu_pengerjaan;
	return $waktu;
	
}


function get_time_tiu ()
{
	$ci =& get_instance();
	$query = $ci->db->query("select waktu_pengerjaan from kategori_ujian where kategori='tiu' ");
	$row = $query->row();
	$waktu = $row->waktu_pengerjaan;
	return $waktu;
	
}


function get_soal()
{
	$ci =& get_instance();
	 foreach($ci->session->userdata("data") as $row)
            {
                $data [] = $row['id'];
            }

	for($i=0;$i<count($data); $i++)
            {
           
                    
                    $query = $ci->db->query("select * from soal_dinas where id ='$data[$i]' order by id desc");
                    foreach ($query->result() as $rows) {
                        $another_question [] = array(
                        	"no"=>($i+1),
                            "id"=>$rows->id
                        );
                }
            }
    return $another_question;
}

function get_tiu()
{
	$ci =& get_instance();
	 foreach($ci->session->userdata("data") as $row)
            {
                $data [] = $row['id'];
            }

	for($i=0;$i<count($data); $i++)
            {
           
                    
                    $query = $ci->db->query("select * from soal_tiu where id ='$data[$i]' order by id desc");
                    foreach ($query->result() as $rows) {
                        $another_question [] = array(
                        	"no"=>($i+1),
                            "id"=>$rows->id
                        );
                }
            }
    return $another_question;
}

function _get_sesi()
{
	$ci =& get_instance();
	$query = $ci->db->get("sesi");

	return $query->result();
}

function get_nomor_soal($id_soal)
{
	$ci =& get_instance();
	$no = 1;
	 foreach($ci->session->userdata("data") as $row)
            {
                $data [$row["id"]] = $no++;
            }

    return $data[$id_soal];
}

function sum_jlh_soal($kode_kategori,$total_input)
{
	$ci =& get_instance();
	$row = $ci->db->query("select sum(persentase) as total_soal from manajemen_ujian where id_kategori_ujian ='$kode_kategori' ");
	$total = $row->row();

	return ($total->total_soal+$total_input);
}

function check_after_before_update($kode_kategori)
{
	$ci =& get_instance();
	$row = $ci->db->query("select sum(persentase) as total_soal from manajemen_ujian where id_kategori_ujian ='$kode_kategori' ");
	$total = $row->row();

	return ($total->total_soal);
}

function _get_data_sesi($id_sesi)
{
	$ci =& get_instance();
	$query = $ci->db->query("select a.*,b.* from sesi a join kategori_ujian b on a.id_kategori_ujian=b.id where a.id_sesi ='$id_sesi' ");
	$row = $query->row();
	return $row;
}

function _get_jenis_soal($id_sesi,$id_kategori_ujian)
{
	$ci =& get_instance();
	$query = $ci->db->query(" select a.* from manajemen_ujian a join kategori_ujian b on a.id_kategori_ujian=b.id join sesi c on c.id_kategori_ujian=b.id where c.id_sesi='$id_sesi' and c.id_kategori_ujian='$id_kategori_ujian' ");
	$row = $query->result();
	return $row;
}


function _get_details_bobot($nip, $id_sesi, $id_jenis_soal)
{
	$ci =& get_instance();
	$query = $ci->db->query("select * from jawaban where nip='$nip' and id_sesi='$id_sesi' ");

	

	if($query->num_rows()>0){
		$usr_jwb = $query->result();
		$bobot = 0;
		foreach ($usr_jwb as $key) {
			
			$querys = $ci->db->query("select * from soal_dinas where id='$key->id_soal' and id_jenis_soal='$id_jenis_soal' ");
			$knci_jwb = $querys->row();
			
			if($querys->num_rows()>0)
			{
				if($knci_jwb->status=="single"){
					if($key->jawaban===$knci_jwb->kunci_jawaban)
				 	{
				 		$bobot += $knci_jwb->bobot;
					}else{
						$bobot += 0;
					}
				}else{
					if($key->jawaban==="A")
				 	{
				 		$bobot += $knci_jwb->bobot_a;
					}else if($key->jawaban==="B"){
						$bobot += $knci_jwb->bobot_b;
					}else if($key->jawaban==="C"){
						$bobot += $knci_jwb->bobot_c;
					}else if($key->jawaban==="D"){
						$bobot += $knci_jwb->bobot_d;
					}else if($key->jawaban==="E"){
						$bobot += $knci_jwb->bobot_e;
					}
					else{
						$bobot += 0;
					}
				}

			}else
			{
				$bobot += 0;
			}
			

		}
		return $bobot;
		
	}else{
		return 0;
	}

}

function get_pegawai_sesi($id_sesi,$id_kategori_ujian)
{
	$ci =& get_instance();
	$query = $ci->db->query("select distinct(a.nip) as nip from jawaban a join sesi b on a.id_sesi=b.id_sesi WHERE a.id_sesi='$id_sesi' and b.id_kategori_ujian='$id_kategori_ujian' ");
	$row = $query->result();
	return $row;
}

function get_detail_pegawai($nip)
{
	$ci =& get_instance();
	$query = $ci->db->get_where('pegawai',array('nip'=>$nip));
	$row = $query->row();
	return $row;
}

function _get_passgrade($id_kategori_ujian,$id_jenis_soal)
{
	$ci =& get_instance();
	$query = $ci->db->get_where('manajemen_ujian',array(
		'id_kategori_ujian'=>$id_kategori_ujian,
		'id_jenis_soal'=>$id_jenis_soal
	));
	$row = $query->row();
	return $row;
}

function get_jawaban_papi($nip,$kat)
{
	$kategori = array("A","B","C","D","E","F","G","I","K","L","N","O","P","R","S","T","V","W","X","Z");
	$ci =& get_instance();
		$get_count = $ci->db->query("select count(jawaban) as nilai from jawaban where nip='$nip' and jawaban='$kat' ");
		$row_dt = $get_count->row();
		
	return $row_dt->nilai;
}
?>
